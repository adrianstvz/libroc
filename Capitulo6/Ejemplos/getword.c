#include <stdio.h>
#include <ctype.h>
#include "getword.h"


#define BUFSIZE 100

static char buf[BUFSIZE];
static int bufp = 0;

int
getword ( char *word, int lim ) {
	
	char c;
	char *w = word;
	
	while ( isspace( c = getch() )) ;
	
	if ( c != EOF ) 
		*w++ = c;
	
	if ( !isalpha(c) ) {
		*w = '\0';
		return c;
	}

	for ( ; --lim > 0; w++ ) {
		if ( !isalnum( *w = getch() )) {
			ungetch(*w);
			break;
		}
	}

	*w = '\0';
	return *word;
}

int getch ( void ) {
	return ( bufp > 0 ) ? buf[--bufp] : getchar() ;
}

void ungetch ( int c ) {
	if ( bufp < BUFSIZE ) bufp++;
	else printf("error ungetch: buffer is full\n");
}
