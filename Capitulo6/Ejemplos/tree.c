#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "getword.h"

#define MAXWORD 100

struct tnode {
	char *word;
	int count;
	struct tnode *left;
	struct tnode *right;
} ;

// Functions
char *mstrdup ( char * ) ;
struct tnode *addtree ( struct tnode *, char * ) ;
struct tnode *talloc ( void ) ;
void treeprint ( struct tnode * ) ;

char *
mstrdup ( char *s ) {

	char *p;

	if (( p = (char *) malloc( ( strlen(s) + 1 ) * sizeof (char) )) != NULL )
		strcpy(p, s);

	return p;
}

struct tnode *
addtree ( struct tnode *t, char *word ) {

	int cond;

	if ( t == NULL ) {
		t = talloc();
		t->word = mstrdup(word);
		t->count = 1;
		t->left = t->right = NULL;
	} else if ( ( cond = strcmp(word, t->word) ) == 0 ) {
		t->count++;
	} else if ( cond < 0 ) {
		t->left = addtree(t->left, word);
	} else {
		t->right = addtree(t->right, word);
	}

	return t;

}

struct tnode *
talloc ( void ) {

	return (struct tnode *) malloc ( sizeof ( struct tnode ) );

}

void
treeprint ( struct tnode *t ) {

	if ( t != NULL ) {
		treeprint(t->left);
		printf("%4d %s\n", t->count, t->word);
		treeprint(t->right);
	}

	free(t);

}

int
main (int argc, char *argv[]) {

	struct tnode *root;
	char word[MAXWORD];

	root = NULL;
	while ( getword(word, MAXWORD) != EOF ) 
		if ( isalpha(*word) )
			root = addtree(root, word);

	treeprint(root);
	return 0;

}


