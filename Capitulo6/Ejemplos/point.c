#include <stdio.h>
#include <stdlib.h>


#define min (a, b) ( (a) < (b) ? (a) : (b) )
#define max (a, b) ( (a) > (b) ? (a) : (b) )

struct point {
	int x;
	int y;
} ;

struct rect {
	struct point p1;
	struct point p2;
} ;



void
main (int argc, char *argv[]) {

	/*
	struct point pt;
	struct point maxpt = { 320, 200 };

	double dist, sqrt(double);

	printf("%d,%d\n", pt.x, pt.y);

	dist = sqrt((double) pt.x * pt.x + (double) pt.y * pt.y);
	*/

	struct rect screen;
	struct point middle;
	struct point makepoint(int, int);

	screen.pt1 = makepoint(0,0);
	screen.pt2 = makepoint(XMAX, YMAX);

	middle = makepoint( ( screen.pt1.x + screen.pt2.x ) / 2,
						( screen.pt1.y + screen.pt2.y ) / 2 );


	struct point origin, *pp;

	p = &origin;
	printf("origin is (%d,%d)\n", (*pp).x, (*pp).y);
	printf("origin is (%d,%d)\n", pp->x, pp->y);

	struct {
		int len;
		char *str;
	} *p ;

	// ++p->len == ++(p->len)
	// p++->len == (p++)->len

}

/****************************************/

int
ptinrect ( struct point p, struct rect r ) {

	return p.x > r.p1.x && p.x < r.p2.x 
		&& p.y > r.p1.x && p.y < r.p2.y;
}
struct point
addpoint( struct point p1, struct point p2 ) {

	p1.x += p2.x;
	p1.y += p2.y;
	return p1;

}

struct point
makepoint ( int x, int y ) {

	struct point tmp;

	tmp.x = x;
	tmp.y = y;

	return tmp;
}

struct rect
canonrect ( struct rect r ) {

	struct rect tmp;

	tmp.p1.x = min (r.p1.x, r.p2.x);
	tmp.p1.y = min (r.p1.y, r.p2.y);
	tmp.p2.x = max (r.p1.x, r.p2.x);
	tmp.p2.y = max (r.p1.y, r.p2.y);

	return tmp;

}

