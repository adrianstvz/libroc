#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "getword.h"

//#define NKEYS ( sizeof keytab / sizeof ( struct key ) )

struct key {
	char *word;
	int count;
} ;

//struct key keytab[NKEYS];

struct key keytab[] = {
	"auto", 0,
	"break", 0,
	"case", 0,
	"char", 0,
	"const", 0,
	"continue", 0,
	"default", 0,
	"do", 0,
	"double", 0,
	"else", 0,
	"float", 0,
	"for", 0,
	"int", 0,
	"if", 0,
	"unsigned", 0,
	"void", 0,
	"volatile", 0,
	"while", 0,
} ;

#define MAXWORD 100
#define NKEYS ( sizeof keytab / sizeof ( *keytab ) )

int getword ( char *, int ) ;
int binsearch ( char *, struct key *, int ) ;

int
main (int argc, char *argv[]) {

	int n;
	char word[MAXWORD];

	while ( getword(word, MAXWORD) != EOF ) {
		if ( isalpha(*word) ) {
			if ( ( n = binsearch(word, keytab, NKEYS)) >= 0 )
				keytab[n].count++;
		}
	}

	for ( n = 0; n < NKEYS; n++ ) {
		if ( keytab[n].count ) {
			printf("%4d %s\n",
					keytab[n].count, keytab[n].word);
		}
	}

	return 0;

}

int
binsearch ( char *word, struct key *tab, int n ) {

	int cond;
	int low, high, mid;

	low = 0;
	high = n - 1;

	while ( low <= high ) {
		mid = ( low + high ) / 2;
		if (( cond = strcmp(word, tab[mid].word )) < 0 ) 
			high = mid - 1;
		else if ( cond > 0 ) 
			low = mid + 1;
		else 
			return mid;
	}
	return -1;

}
