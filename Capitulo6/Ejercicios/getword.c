#include <stdio.h>
#include <ctype.h>
#include "getword.h"

#define BUFSIZE 100

void skipline ( void ) ;

static char buf[BUFSIZE];
static int bufp = 0;

int
getword ( char *word, int lim ) {
	
	char c;
	char *w = word;
	static int status = CODE;
	
	while ( isspace( c = getch() )) ;
	
	if ( c != EOF ) 
		*w++ = c;
	
	// Check if is special character
	if ( !isalpha(c) ) {
		switch(c) {
			case '\n':	// Newline
				if ( status != COMMENT )
					status = CODE;
				break;
			case '/':	// Check if comment
				if ( status != COMMENT ) {
					if ( ( c = getch() ) == '/' )
						skipline();
					else if ( c == '*' )
						status = COMMENT;
					else ungetch(c);
				}
				break;
			case '*':	// Multiline comment end
				if ( ( c = getch() ) == '/' && status == COMMENT )
					status = CODE;
				else ungetch(c);
				break;
			case '"': // Skip string constant
				while ( ( c = getch() ) != '"' && c != EOF ) 
					*w++ = c;
				ungetch(c);
			case '#': // Skip PREPROC
				skipline();
		}

		*w = '\0';
		return c;
	}

	for ( ; --lim > 0; w++ ) {
		if ( (!isalnum( *w = getch() ) && *w != '_' && *w != '-' ) || status == COMMENT ) {
			ungetch(*w);
			break;
		}
	}

	*w = '\0';
	return *word;
}

int getch ( void ) {
	return ( bufp > 0 ) ? buf[--bufp] : getchar() ;
}

void ungetch ( int c ) {
	if ( bufp < BUFSIZE ) buf[bufp++];
	else printf("error ungetch: buffer is full\n");
}

void
skipline ( void ) {

	char c;

	while ( ( c = getch () ) != '\n' && c != EOF ) ;

	ungetch(c);

}
