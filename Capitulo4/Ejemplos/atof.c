#include <stdio.h>
#include <ctype.h>

#define MAXLINE 1000

double atof (char *) ;
int mgetline (char *, int);

int
mgetline (char *s, int lim) {
	
	int i = 0;
	char c;

	while ( i<lim && ( c = getchar() ) != EOF && c != '\n' )
		s[i++] = c;

	if ( c == '\n' ) s[i++] = c;
	s[i] = '\0';

	return i;

}


double
atof (char *s) {

	double val, power;
	int i, sign;

	for ( i=0; isspace(s[i]); i++ ) ;

	sign = (s[i] == '-') ? -1 : 1;

	if ( s[i] == '+' || s[i] == '-' ) i++;

	for ( val = 0.0; isdigit(s[i]); i++ )
		val = 10.0 * val + (s[i] -'0');

	if ( s[i] == '.' ) i++;

	for ( power = 1.0; isdigit(s[i]); i++ ) {
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}

	return sign * val / power;
}

void
main (int argc, char *argv[]) {

	char line[MAXLINE];

	while ( mgetline(line, MAXLINE) > 0 )
		printf("%f\n", atof(line));

}

