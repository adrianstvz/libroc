#include <stdio.h>

#define MAXVAL 100

int sp = 0;
double stack[MAXVAL];

void
push (double f) {
	if ( sp < MAXVAL )
		stack[sp++] = f
	else
		printf("error: stack is full, can't push %g\n", f);
}

double
pop (void) {
	if (sp > 0 )
		return stack[--sp];
	else {
		printf("error: stack is empty\n");
		return 0.0;
	}
}
