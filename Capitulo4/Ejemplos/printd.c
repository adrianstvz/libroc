#include <stdio.h>

void printd (int) ;

void
printd (int n) {
	
	if (n<0) {
		putchar('-');
		n = -n;
	}

	if (n/10) printd(n/10);

	putchar('0' + n%10);

}

void
main (int argc, char *argv[]) {

	printd(-10014124);
	putchar('\n');

	printd(345643);

}

