#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

void mqsort (int *, int, int) ;
void swap (int *, int, int) ;

void
aprint (int *v, int l, int r ) {

	for ( int i=l; i<=r; i++ ) 
		printf("%d%c", v[i], (i == r)?'\n':',') ;
}

void
swap (int *v, int i, int j) {
	int tmp ;

	tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
}

void
mqsort (int *v, int left, int right) {

	int i, last;

	if (left >= right) // If less than 2 elements return
		return;
	
	aprint(v, left, right);	
	swap(v, left, (left + right)/2); 
	aprint(v, left, right);	
	last = left;

	for (i=left+1; i <= right; i++){
		if (v[i] < v[left])
			swap(v, ++last, i);
	}

	aprint(v, left, right);	
	swap(v, left, last);
	aprint(v, left, right);	
	mqsort(v, left, last-1);
	mqsort(v, last+1, right);

}

void
main (int argc, char *argv[]) {

	int i;
	int vector[SIZE];

	for ( i=0; i<SIZE; i++ ) vector[i] = rand() % SIZE;

	for ( i=0; i<SIZE; i++ ) 
		printf("%d%c", vector[i], (i == SIZE -1)?'\n':',') ;

	mqsort(vector, 0, SIZE-1);
		
	for ( i=0; i<SIZE; i++ ) 
		printf("%d%c", vector[i], (i == SIZE -1)?'\n':',') ;

}

