#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXOP 100
#define NUMBER '0'
#define MAXVAL 100
#define BUFSIZE 100

int getop (char *) ;
char getch (void) ;
void ungetch (char) ;
void push (double) ;
double pop (void) ;

int sp = 0;
int bufp = 0;

double stack[MAXVAL];
char buf[BUFSIZE];

void
push (double f) {
	if ( sp < MAXVAL )
		stack[sp++] = f;
	else
		printf("error: stack is full, can't push %g\n", f);
}

double
pop (void) {
	if (sp > 0 )
		return stack[--sp];
	else {
		printf("error: stack is empty\n");
		return 0.0;
	}
}

char
getch (void) {
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch (char c) {
	if ( bufp < BUFSIZE ) buf[bufp++] = c;
	else printf ("ungetch: buffer is full\n");
}

int
getop (char *s) {

	int i = 0;
	char c;

	while (( s[0] = c = getch()) == ' ' || c == '\t' ); // Skip blanks

	s[1] = '\0';

	if ( !isdigit(c) && c != '.' ) return c; // Not a number

	if ( isdigit(c) ) 
		while ( isdigit(s[++i] = c = getch()) );

	if ( c == '.' )
		while ( isdigit(s[++i] = c = getch()) );

	s[i] = '\0';

	if ( c != EOF )
		ungetch(c);

	return NUMBER;

}

void
main (int argc, char *argv[]) {

	int type;
	double op2;
	char s[MAXOP];

	while (( type = getop(s) ) != EOF ) {
		switch (type) {
			case NUMBER:
				push(atof(s));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if ( op2 != 0.0 ) 
					push(pop() / op2);
				else
					printf("error: illegal expresion, division by zero\n");
				break;
			case '\n':
				printf("\t%.8g\n", pop());
				break;
			default:
				printf("error: unknown command '%s'\n", s);
				break;
		}
	}

}

