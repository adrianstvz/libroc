#include <stdio.h>

#define BUFSIZE 100

char buf[BUFSIZE];
int bufp = 0;

char
getch (void) {
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch (char c) {
	if ( bufp < BUFSIZE ) buf[bufp++] = c;
	else printf ("ungetch: buffer is full\n");
}

