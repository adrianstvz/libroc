#include <stdio.h>

#define MAXLINE 1000

int mgetline (char *, int);
int strindex (char *, char *);

int
mgetline (char *s, int lim) {

	int i = 0;
	char c;

	while ( i<lim && ( c = getchar() ) != EOF && c != '\n' ) s[i++] = c;

	if ( c == '\n' ) s[i++] = c;
	s[i] = '\0';

	return i;

}

int
strindex (char *s, char *t) {
	
	int i, j, k;
	char c;

	for ( i = 0; s[i] != '\0'; i++ ) {
		for ( j = 0,  k = i; t[j] != '\0' && t[j] == s[k]; j++, k++ );

		if (t[j] == '\0' && k > 0 ) return i;
	}

	return -1;

}

void
main (int argc, char *argv[]) {

	char match[] = "hell";
	char line[MAXLINE];
	int p;

	while ( mgetline(line, MAXLINE) > 0 ) 
		if ( (p = strindex(line, match)) >= 0 ) printf("> %s", line);

}

