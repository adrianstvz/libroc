#include <stdio.h>
#include <ctype.h>

#define MAXLINE 1000

double atof (char *) ;
int mgetline (char *, int);
double mpow (int, int) ;

int
mgetline (char *s, int lim) {
	
	int i = 0;
	char c;

	while ( i<lim && ( c = getchar() ) != EOF && c != '\n' )
		s[i++] = c;

	if ( c == '\n' ) s[i++] = c;
	s[i] = '\0';

	return i;

}

double
mpow (int base, int n) {

	double pow = 1;

	while (n-- > 0) pow *= base; // Calculate if n>0
	while (++n < 0) pow /= base; // Calculate if n<0
		
	return pow;

}

double
atof (char *s) {

	double val, power;
	int i, sign, psign, exp;

	for ( i=0; isspace(s[i]); i++ ) ;

	sign = (s[i] == '-') ? -1 : 1;

	if ( s[i] == '+' || s[i] == '-' ) i++;

	for ( val = 0.0; isdigit(s[i]); i++ )
		val = 10.0 * val + (s[i] -'0');

	if ( s[i] == '.' ) i++;

	for ( power = 1.0; isdigit(s[i]); i++ ) {
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}

	// If scientific notation then calculate the new power
	if ( s[i] == 'e' || s[i] == 'E' ) {
		i++;

		psign = (s[i] == '-') ? -1 : 1;
		if ( s[i] == '+' || s[i] == '-' ) i++;

		for ( exp = 0; isdigit(s[i]); i++ )
			exp = 10*exp + (s[i] -'0');

		power *= 1.0 / mpow(10, psign*exp );
	}

	return sign * val / power;
}

void
main (int argc, char *argv[]) {

	char line[MAXLINE];

	while ( mgetline(line, MAXLINE) > 0 )
		printf("%.10f\n", atof(line));

}

