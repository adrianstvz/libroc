#include <stdio.h>

#define swap(t,x,y) { 	t tmp = x; \
						x = y; \
						y = tmp; }

void
main (int argc, char *argv[]) {

	int x, y;
	
	x = 887;
	y = 2321;

	printf("x: %d y: %d\n", x, y);
	swap(int, x, y);
	printf("x: %d y: %d\n", x, y);


}

