#include <stdio.h>
#include <ctype.h>
#include "calc.h"

char
getop (char *s) {

	int i = 0;
	char c;
	static char lastc=0;

	if ( lastc ) {
		c = lastc;
		lastc = 0;
	} else
		c = getch();
	
	while (( s[0] = c ) == ' ' || c == '\t' ) c = getchar(); // Skip blanks

	s[1] = '\0';

	if ( !isdigit(c) && c != '.' ) return c; // Not a number

	if ( isdigit(c) ) 
		while ( isdigit(s[++i] = c = getchar()) );

	if ( c == '.' )
		while ( isdigit(s[++i] = c = getchar()) );

	s[i] = '\0';

	if ( c != EOF )
		lastc = c;

	return NUMBER;

}
