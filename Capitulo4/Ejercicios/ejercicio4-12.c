#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 100

void ritoa (char *, int) ;

void 
ritoa (char *s, int n) {

	static int index;	// static variable so value is shared 

	if ( n/10 ) ritoa(s, n/10); // If not last digit call recursive
	else {
		index = 0;				// If last digit then set index to 0
		if ( n<0 ) {			// If negative add a - at the beginning
			s[index++]='-';
		}
	}

	s[index++] = '0' + abs(n)%10; // Add current digit
	s[index] = '\0';			 // And add end of string after

}

void
main (int argc, char *argv[]) {

	char s[MAXLINE];

	ritoa(s,11231);

	printf("%s\n", s);
		
	ritoa(s,-9989);
	printf("%d -> %s\n", -9989, s);

}

