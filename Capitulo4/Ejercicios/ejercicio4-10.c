#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXOP 100
#define NUMBER '0'
#define MAXVAL 100
#define MAXLINE 1000

int getop (char *) ;
int mgetline (char *, int) ;
void push (double) ;
double pop (void) ;

int sp = 0;
int li = 0;

double stack[MAXVAL];
char line[MAXLINE];

void
push (double f) {
	if ( sp < MAXVAL )
		stack[sp++] = f;
	else
		printf("error: stack is full, can't push %g\n", f);
}

double
pop (void) {
	if (sp > 0 )
		return stack[--sp];
	else {
		printf("error: stack is empty\n");
		return 0.0;
	}
}

int
getop (char *s) {

	int sign, i = 0;
	char c;

	if ( line[li] == '\0' ) {
		if ( mgetline(line, MAXLINE)) li = 0;
		else return EOF;
	}

	while (( s[0] = c = line[li++]) == ' ' || c == '\t' ); // Skip blanks

	s[1] = '\0';
		
	if ( !isdigit(c) && c != '.' && c != '-' ) return c; // Not a number

	if ( c == '-' ) {
		if ( ! isdigit(s[++i] = c = line[li++] ) && c != '.' ) {
			s[1] = '\0';
			li--;
			return '-';
		}
	}

	if ( isdigit(c) ) {
		while ( isdigit(s[++i] = c = line[li++]) );
	}

	if ( c == '.' )
		while ( isdigit(s[++i] = c = line[i++]) );

	s[i] = '\0';

	li--;

	return NUMBER;

}

int
mgetline (char *s, int lim) {

	int i = 0;
	char c;

	while ( i<lim && ( c = getchar() ) != EOF && c != '\n' ) s[i++] = c;

	if ( c == '\n' ) s[i++] = c;
	s[i] = '\0';

	return i;

}

void
main (int argc, char *argv[]) {

	int type;
	double op2;
	char s[MAXOP];

	while (( type = getop(s) ) != EOF ) {
		switch (type) {
			case NUMBER:
				push(atof(s));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if ( op2 != 0.0 ) 
					push(pop() / op2);
				else
					printf("error: illegal expresion, division by zero\n");
				break;
			case '%':
				op2 = pop();
				if ( op2 != 0.0 ) 
					push((int)pop() % (int)op2);
				else
					printf("error: illegal expresion, modulo by zero\n");
				break;
			case '\n':
				printf("\t%.8g\n", pop());
				break;
			default:
				printf("error: unknown command '%s'\n", s);
				break;
		}
	}


}
