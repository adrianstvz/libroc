#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

#define MAXOP 100
#define NUMBER '0'
#define FUNCTION '1'
#define VAR '2'
#define MAXVAL 100
#define MAXVAR 'Z'-'A'
#define BUFSIZE 100

int getop (char *) ;
char getch (void) ;
void ungetch (char) ;
void clear (void) ;
double head (void); 
void mfunc (char *); 
void push (double) ;
double pop (void) ;

int sp = 0;
int bufp = 0;

double stack[MAXVAL];
char buf[BUFSIZE];

void
push (double f) {
	if ( sp < MAXVAL )
		stack[sp++] = f;
	else
		printf("push: stack is full, can't push %g\n", f);
}

double
pop (void) {
	if (sp > 0 )
		return stack[--sp];
	else {
		printf("pop: stack is empty\n");
		return 0.0;
	}
}

double
head (void) {
	if (sp > 0 )
		return stack[sp-1];
	else {
		printf("head: stack is empty\n");
		return 0.0;
	}
}

void
clear (void) {

	sp = 0;

}

void
mfunc (char *s) {

	double e1;


	if ( ! strcmp(s, "sin") ) {
		push(sin(pop()));
	}
	else if ( ! strcmp(s, "cos") ) {
		push(cos(pop()));
	}
	else if ( ! strcmp(s, "tan") ) {
		push(tan(pop()));
	}
	else if ( ! strcmp(s, "exp") ) {
		push(exp(pop()));
	}
	else if ( ! strcmp(s, "log") ) {
		push(log(pop()));
	}
	else if ( ! strcmp(s, "sqrt") ) {
		push(sqrt(pop()));
	}
	else if ( ! strcmp(s, "pow") ) {
		e1 = pop();
		push(pow(pop(), e1));
	}
	else 
		printf("mfunc: Unknown function %s\n", s);

}

char
getch (void) {
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch (char c) {
	if ( bufp < BUFSIZE ) buf[bufp++] = c;
	else printf ("ungetch: buffer is full\n");
}

int
getop (char *s) {

	int sign, i = 0;
	char c;

	while (( s[0] = c = getch()) == ' ' || c == '\t' ); // Skip blanks

	s[1] = '\0';

	if (islower(c)) {
		while ( islower(s[++i] = c = getch()) );

		s[i] = '\0';

		if ( c != EOF )
			ungetch(c);

		if ( strlen(s) > 1 ) // If string
			return FUNCTION;
		else				// If single character
			return s[i-1];
	}

	if ( isupper(c) ) {
		return VAR;
	}

	if ( !isdigit(c) && c != '.' && c != '-' ) return c; // Not a number

	if ( c == '-' ) { // Negative number
		if ( ! isdigit( c = getch()) && c != '.' ) { 
			if ( c != EOF ) ungetch(c);
			return '-';
		}
		else s[++i] = c;
	}

	if ( isdigit(c) ) 
		while ( isdigit(s[++i] = c = getch()) );

	if ( c == '.' )
		while ( isdigit(s[++i] = c = getch()) );

	s[i] = '\0';

	if ( c != EOF )
		ungetch(c);

	return NUMBER;

}

void
main (int argc, char *argv[]) {

	int type;
	double op2, r, e1, e2, varray[MAXVAR];
	char vname, s[MAXOP];

	for ( int i=0; i<MAXVAR; i++ ) varray[i] = 0;

	while (( type = getop(s) ) != EOF ) {
		switch (type) {
			case FUNCTION:
				mfunc(s);
				break;
			case NUMBER:
				push(atof(s));
				break;
			case VAR:
				vname = s[0];
				push(varray[vname-'A']);
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op2 = pop();
				push(pop() - op2);
				break;
			case '/':
				op2 = pop();
				if ( op2 != 0.0 ) 
					push(pop() / op2);
				else
					printf("error: illegal expresion, division by zero\n");
				break;
			case '%':
				op2 = pop();
				if ( op2 != 0.0 ) 
					push((int)pop() % (int)op2);
				else
					printf("error: illegal expresion, modulo by zero\n");
				break;
			case '=': // Store value on 
				pop();
				if ( isupper(vname) ) // If is upper letter store value on array
					varray[vname-'A'] = pop();
				else printf("error: invalid variable \n");
				break;
			case '?': // Prints the top of the stack
				printf("\t%.8g\n", head());
				break;
			case 'c': // Clears the stack
				clear();
				break;
			case 'd': // Duplicates the top element
				e1 = head();
				push(e1);
				break;
			case 'r': // Adds last result to the stack
				push(r);
				break;
			case 's': // Swaps the two top elements
				e1 = pop();
				e2 = pop();
				push(e1);
				push(e2);
				break;
			case '\n':
				r = pop(); // Store result
				printf("\t%.8g\n", r );
				break;
			default:
				printf("error: unknown command '%s'\n", s);
				break;
		}
		
		vname = s[0];
	}

}

