#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXVAL 100
#define BUFSIZE 100

char getch (void) ;
void ungetch (char) ;
void ungets (char *) ;

int bufp = 0;

char buf[BUFSIZE];

char
getch (void) {
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch (char c) {
	if ( bufp < BUFSIZE ) buf[bufp++] = c;
	else printf ("ungetch: buffer is full\n");
}

void
ungets (char *s) {
	int i = strlen(s);

	/* First version accesing buf and bufp
	while ( i > 0 && bufp < BUFSIZE ) buf[bufp++] = s[--i];
	if ( bufp >= BUFSIZE ) printf ("ungetch: buffer is full\n");
	*/
	/* Second version just using ungetch, safer and simpler */
	while ( i > 0 ) ungetch(s[--i]);

}

void
main (int argc, char *argv[]) {

	char line[BUFSIZE];
	int i;
	char c;

	i = 0;
	while ( (c = getch()) != '\n' && c != EOF && c != ' ' ) {
		line[i++] = c;
	}
	if ( c == '\n' || c == ' ' ) line[i++] = c;
	line[i] = '\0';

	printf("%s", line);

	ungets(line);

	i = 0;
	while ( (c = getch()) != EOF ) {
		line[i++] = c;
		if (c == ' ' || c == '\n') {
			line[i] = '\0';
			printf("%s", line);
			i = 0;
		}
	}
	
}

