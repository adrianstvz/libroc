#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXVAL 100
#define BUFSIZE 100

char getch (void) ;
void ungetch (char) ;
void ungets (char *) ;

// Since EOF has value -1, we change the buffer limit to -2
char buf = -2;

/* The array version just werks

char bufp = 0;
char buf[BUFSIZE];

char
getch (void) {
	return (bufp > 0) ? buf[--bufp] : getchar();
}

void
ungetch (char c) {
	if ( bufp < BUFSIZE ) buf[bufp++] = c;
	else printf ("ungetch: buffer is full\n");
}
*/

char
getch (void) {
	char c;
	c = (buf >= -1) ? buf : getchar();
	buf = -2;
	return c;
}

void
ungetch (char c) {
	if ( buf < -1 ) buf = c;
	else printf ("ungetch: buffer is full\n");
}

void
main (int argc, char *argv[]) {

	ungetch(EOF);
	printf("EOF int value: %d \n", EOF);
	printf("ungetch(EOF); getch(); '%s'\n", (getch() == EOF) ? "EOF" : "Not EOF");

}

