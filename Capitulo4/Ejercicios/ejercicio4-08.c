#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXVAL 100
#define BUFSIZE 100

char getch (void) ;
void ungetch (char) ;
void ungets (char *) ;

char buf = -1;

char
getch (void) {
	char c;
	c = (buf > 0) ? buf : getchar();
	buf = -1;
	return c;
}

void
ungetch (char c) {
	if ( buf < 0 ) buf = c;
	else printf ("ungetch: buffer is full\n");
}

void
main (int argc, char *argv[]) {

}

