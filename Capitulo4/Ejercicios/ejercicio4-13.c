#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 100

void rreverse (char *) ;

void 
rreverse (char *s) {

	static int j, i;	// static variable so value is stored between calls 
	char c;

	if ( (c = s[j++] ) != '\0' ) rreverse(s); // If not last character call recursive
	else {
		j = 0;
		i = 0;	// If last char then set indexes to 0
		return;	// return
	}

	s[i++] = c;
	s[i] = '\0';

}

void
main (int argc, char *argv[]) {

	char s1[] = "ABCDEF";
	char s2[] = "opqrs123";

	rreverse(s1);

	printf("%s\n", s1);
		
	rreverse(s2);
	printf("%s\n", s2);

}

