#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

enum { LOW, UP } ;

void
prepare_name ( char *s ) {
	
	char *start, *p;

	for ( start = p = s; *s; s++ ) {

		if ( !isalnum(*s) && *s != '_' && *s != '-' )
			p = s+1;

	}

	s = start;
	strcpy(s,p);

}

int
main (int argc, char *argv[]) {
	
	char c, op, *s;

	s = (char *) malloc ( ( strlen(*argv) + 1 ) * sizeof(char) );

	strcpy(s, *argv);

	prepare_name(s);

	if ( !strcmp(s,"lower") ) op = LOW;
	else if ( !strcmp(s,"upper") ) op = UP;
	else {
		printf("Unknown program\n");
		return 1;
	}

	while ( ( c = getchar() ) != EOF ) {
		switch(op) {
			case LOW:
				putchar(tolower(c));
				break;
			case UP:
				putchar(toupper(c));
				break;
			default:
				break;
		}
	}
	return 0;

}

