#include <stdio.h>
#include <stdlib.h>

unsigned setbits (unsigned, int, int, unsigned) ;
unsigned invert (unsigned, int, int) ;
void print_bits (unsigned) ;

unsigned
setbits (unsigned x, int p, int n, unsigned y) {
/* This function changes n bits from p position on x using the n most right bits from y */

	// Remove the p+1 most right bits from x 
	// Get the n most right bits from y 
	// Add the new bits to x starting on p
	// Add the rest of the x bits again, starting on p-n+1
	return (( x & ( ~0 << p+1 )) | ( y & ~( ~0 << n ) << p )) | ( x & ~( ~0 << (p - n + 1 ) )) ;

}

unsigned
invert (unsigned x, int p, int n) {
/* This function reverses n bits from p position */

	// Remove the p+1 most right bits from x 
	// Reverse x and extract the n bits to the right of p
	// Add the new bits to x starting on p
	// Add the rest of the x bits again, starting on p-n+1
	return (( x & ( ~0 << p )) | (((~x >> ( p - n )) & ~( ~0 << n )) << p )) | ( x & ~( ~0 << (p - n ) )) ;

}

void
print_bits (unsigned x) {
/* This function prints the binary value of x */

	unsigned rev = 0;
	int i=0;

	while ( x ) {
		rev = (rev << 1) + (x & 1);
		x >>= 1;
		i++;
	}

	for ( ; i; i-- ) {
		printf("%u", rev & 1);
		rev >>= 1;
	}
	printf("\n");

}

void
main (int argc, char *argv[]){

	unsigned x=0xff;
	unsigned y=0x80;

	int p=4, n=3;

	printf("setbits: %u, %u, %u, %u -> %u\n", x, p, n, y, setbits(x,p,n,y));
	print_bits(x);
	print_bits(y);
	print_bits(setbits(x,p,n,y));

	printf("invert: %u, %u, %u -> %u\n", x, p, n, invert(x,p,n));
	print_bits(x);
	print_bits(invert(x,p,n));
}

