#include <stdio.h>
#include <stdlib.h>

void print_bits (unsigned) ;
int bitcount (unsigned) ;
int nbitcount (unsigned) ;

void
print_bits (unsigned x) {
/* This function prints the binary value of x */

	unsigned rev = 0;
	int i=0;

	while ( x || (i % 4) ) {
		rev = (rev << 1) + (x & 1);
		x >>= 1;
		i++;
	}

	if (!rev) printf("0000");

	for ( ; i; i-- ) {
		printf("%u", rev & 1);
		rev >>= 1;
	}
	printf("\n");

}

int
bitcount (unsigned x) {

	unsigned c;

	for ( c=0; x > 0; x>>=1) c += x&1;

	return c;

}

int
nbitcount (unsigned x) {

	unsigned c;

	for ( c=0; x > 0; x &= (x-1) ) c++;

	return c;

}

void
main (int argc, char *argv[]){

	int x=0xfc;
	int y=-0xfc;


	// TEST
	printf("Since the -1 representation on a two's complement system: ");
	print_bits(-1);

	printf("The substraction is just an addition, which in the end changes all the zeroes to the rightmost 1bit to 1, and said bit to 0\n");
	printf("So:      "); print_bits(x);
	printf("Becomes: "); print_bits(x-1);

	printf("We can use this new value as a mask to remove the rightmost 1-bit in the original: ");

	print_bits(x & (x-1));


	printf("\nThe same happens with negative numbers\n");
	print_bits(y);
	print_bits(y-1);
	print_bits(y & (y-1));

	printf("This allows for a faster bitcount since we will jump directly to the next 1\n");
	printf("%d: %d, %d\n", x, bitcount(x), nbitcount(x));
	printf("%d: %d, %d\n", y, bitcount(y), nbitcount(y));

}

