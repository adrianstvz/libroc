
#include <stdio.h>

char lower (char);

char
lower (char c) {

	return ( c >= 'A' && c <= 'Z' ) ? c + 'a' - 'A' : c;
}

void
main (int argc, char *argv[]) {

	printf("%c \n", lower('a'));
	printf("%c \n", lower('A'));
	printf("%c \n", lower('!'));

}


