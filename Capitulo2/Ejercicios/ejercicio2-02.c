#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000

int
mgetline (char *s, int lim) {

	int i;
	char c;

	for ( i = 0; i<lim-1; i++){
		if (( c = getchar() ) == EOF ) break;

		if ( c  == '\n' ) {
			s[i++] = c;
			break;
		}

		s[i] = c;
	}

	s[i] = '\0';

	return i;
}

void
main (int argc, char *argv[]){

	int i;
	int lim = MAXLINE;

	char line[MAXLINE];

	while ( mgetline(line, lim) ) printf("%s", line);

}

