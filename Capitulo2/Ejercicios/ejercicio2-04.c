#include <stdio.h>

#define MAXLINE 1000

void msqueeze (char *, char *);

void
msqueeze (char *s, char *t) {
/* This function removes from the first string all the characters found on the second string */
	
	int i, j, k;

	for ( i = k = 0 ; s[i] != '\0'; i++ ) {

		for ( j = 0; (s[i] != t[j]) && (t[j] != '\0'); j++ ) ;

		if ( s[i] != t[j] ) s[k++] = s[i];

	}
	
	s[k] = '\0';
}

void
main (int argc, char *argv[]){

	char s[MAXLINE] = "abcdefgHIJKLMNEDDDDDDDDDDB";

	msqueeze(s, "bcDEfghiJKl");

	printf("%s\n", s);

}

