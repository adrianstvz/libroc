#include <stdio.h>

#define MAXLINE 1000

int any (char*, char*);

int
any (char *s, char *t) {
/* This function returns the position of the first character in string A included in string B */

	int i, j;
	
	for ( i=0; s[i] != '\0'; i++) {
		for ( j=0; (s[i] != t[j]) && (t[j] != '\0'); j++);

		if ( s[i] == t[j] ) return i;
	}

	return -1;
}

void
main (int argc, char *argv[]) {

	printf("%d\n", any("HELLO", "WORLD"));

	printf("%d\n", any("ABC", "DEFG"));

}

