#include <stdio.h>

int htoi (char *);
char lower (char);

int
htoi (char *s) {

	char c;
	int i = 0, n = 0;

	if ( s[0] == '0' && lower(s[1]) == 'x' ) i = 2;

	for (i ; ((c = lower(s[i])) >= '0' && c <= '9' ) || ( c >= 'a' && c <= 'f' ) ; i++) {
		n = n*16 + (
				( c >= '0' && c <= '9') * (c - '0')
				+ (c >= 'a' && c <= 'f') * (c - 'a' + 10) );
	}

	return n;
}

char
lower (char c) {
	c += ( c >= 'A' && c <= 'Z' ) * ( 'a' - 'A');
}

void
main (int argc, char *argv[]) {

	printf("%d = %d \n", 0xf8f, htoi("0XF8F"));

}

