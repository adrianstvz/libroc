#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

void
print_calculated () {

	printf("\n## ------------------------ \n");
	printf("Calculated limits for integer types:\n\n");

	// To calculate the values:
	// we get the max value through the complement of 0 and shifting to the right
	// 	- ~0 -> 1111 1111 -> unsigned 255, signed -1
	// 	- ~0 >> 1 -> 0111 1111 -> unsigned 127, signed 127
	// we can get the minimun using overflow (adding 1) or with the complement
	// 	- ~(~0 >> 1)  -> ~0111 1111 -> 1000 0000 -> unsigned 128, signed -128
	// 	- ~0 >> 1 + 1 -> 0111 1111 + 1 -> 1000 0000 -> unsigned 128, signed -128
	printf("Max char: %d\n", (char) ((unsigned char) ~0 >> 1));
	printf("Min char: %d\n", (char) ~((unsigned char) ~0 >> 1));

	printf("Max short: %d\n", (short) ((unsigned short) ~0 >> 1));
	printf("Min short: %d\n", (short) (((unsigned short) ~0 >> 1) + 1));

	printf("Max int: %d\n", (int) ((unsigned int) ~0 >> 1));
	printf("Min int: %d\n", (int) (((unsigned int) ~0 >> 1) + 1));

	printf("Max long: %ld\n", (long) ((unsigned long) ~0 >> 1));
	printf("Min long: %ld\n", (long) (((unsigned long) ~0 >> 1) + 1));

	printf("\nMin unsigned: %u\n", 0);

	printf("Max unsigned char: %d\n", (unsigned char) ~0 );
	printf("Max unsigned short: %u\n", (unsigned short) ~0 );
	printf("Max unsigned int: %u\n", (unsigned int) ~0 );
	printf("Max unsigned long: %lu\n", (unsigned long) ~0 );

	printf("\nCalculated limits for floating point types:\n\n");

	printf("Max float: %f\n", FLT_MAX);
	printf("Min float: %f\n", -FLT_MAX);

	printf("Max double: %f\n", DBL_MAX);
	printf("Min double: %f\n", -DBL_MAX);

}

void
print_constants () {

	printf("Limits for integer types as in limits.h:\n\n");

	printf("Max char: %d\n", CHAR_MAX);
	printf("Min char: %d\n", CHAR_MIN);

	printf("Max short: %d\n", SHRT_MAX);
	printf("Min short: %d\n", SHRT_MIN);

	printf("Max int: %d\n", INT_MAX);
	printf("Min int: %d\n", INT_MIN);

	printf("Max long: %ld\n", LONG_MAX);
	printf("Min long: %ld\n", LONG_MIN);

	printf("\nMin unsigned: %u\n", 0);

	printf("Max unsigned char: %u\n", UCHAR_MAX);
	printf("Max unsigned short: %u\n", USHRT_MAX);
	printf("Max unsigned int: %u\n", UINT_MAX);
	printf("Max unsigned long: %lu\n", ULONG_MAX);

	printf("\nLimits for floating point types as in float.h:\n\n");

	printf("Max float: %f\n", FLT_MAX);
	printf("Min float: %f\n", -FLT_MAX);

	printf("Max double: %f\n", DBL_MAX);
	printf("Min double: %f\n", -DBL_MAX);

}

void
main (int argc, char *argv[]){

	print_constants();

	print_calculated();
}

