#include <stdio.h>
#include <stdlib.h>

int
matoi (char *s) {

	int i, n;

	n = 0;

	for (i = 0; s[i] >= '0' && s[i] <= '9' ; i++) 
		n = 10*n + (s[i] - '0');

	return n;
}

void
main (int argc, char *argv[]){

	printf("%d - %d - %d\n", matoi("1234"), matoi("11as999"), matoi("a11212"));

}

