#include <stdio.h>

#define MAXLINE 1000

#define STR1 "Hello World!"
#define STR2 "Rocinante"
#define STR3 "I am a very long string with many words and characters and which actually has this exact number of characters"

int matoi (char *);
int is_leap (int);
char mlower (char);
int rand ();
void srand (unsigned int);
int mstrlen (char *);
void mstrcat (char *, char *);
unsigned getbits (unsigned, int, int);
int bitcount (unsigned);

unsigned long int next = 1;

// {{{ FUNCTIONS
int
matoi (char *s) {

	int i, n;

	n = 0;

	for (i = 0; s[i] >= '0' && s[i] <= '9' ; i++) 
		n = 10*n + (s[i] - '0');

	return n;
}

int
is_leap (int year) {

	return ( year % 4 == 0 ) && ( year % 100 != 0 ) || ( year % 400 == 0 );

}

char
mlower (char c) {

	if ( ( c >= 'A' && c <= 'Z' ))
		c += 'a' - 'A' ;

	return c;
}

int
rand () {
	next = next * 110351245 + 12345;
	return (unsigned int) (next / 65536) % 32768;
}

void
srand (unsigned int seed) {

	next = seed;
}

void
mstrcat (char *s, char *t) {
	
	int i, j;

	i = j = 0;

	for ( i ; s[i] != '\0'; i++ ) ;

	while ( ( s[i++] = t[j++] ) != '\0' ) ;

}

int
mstrlen (char *s) {

	int i=0;

	while (s[i++] != '\0');

	return --i;
}

unsigned 
getbits (unsigned x, int p, int n){

	return (x >> ( p+1-n )) & ~(~0 << n );

}

int
bitcount (unsigned x) {
	int count;

	for ( count = 0; x != 0; x >>= 1 )
		count += (x & 0x1);

	return count;
}

// }}}

// MAIN

void
main (int argc, char *argv[]){

	printf("%u \n", getbits(11,4,3));

	printf("%d %d \n", 0xff, bitcount(0xff));
}
