#include <stdio.h>
#include <stdlib.h>

int
is_leap (int year) {

	return ( year % 4 == 0 ) && ( year % 100 != 0 ) || ( year % 400 == 0 );

}

void
main (int argc, char *argv[]){

	int year;

	year = 2100;

	if ( is_leap(year) ) printf("Year %d is leap\n", year);
	else printf("Year %d is not leap\n", year);
}

