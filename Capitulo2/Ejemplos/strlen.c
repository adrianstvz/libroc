#include <stdio.h>
#include <stdlib.h>

#define STR1 "Hello World!"
#define STR2 "Rocinante"
#define STR3 "I am a very long string with many words and characters and which actually has this exact number of characters"

int mstrlen (char *);

int
mstrlen (char *s) {

	int i=0;

	while (s[i++] != '\0');

	return --i;
}

void
main (int argc, char *argv[]){

	printf("%s -> %d\n", STR1, mstrlen(STR1));
	printf("%s -> %d\n", STR2, mstrlen(STR2));

	printf("%s -> %d\n", STR3, mstrlen(STR3));

}
