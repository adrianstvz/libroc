#include <stdio.h>
#include <stdlib.h>

char
mlower (char c) {

	if ( ( c >= 'A' && c <= 'Z' ))
		c += 'a' - 'A' ;

	return c;
}

void
main (int argc, char *argv[]){

	char c;

	while ( (c = getchar()) != EOF)
		putchar(mlower(c));

}

