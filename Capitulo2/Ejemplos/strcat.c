#include <stdio.h>

#define MAXLINE 1000

void mstrcat (char *, char *);

void
mstrcat (char *s, char *t) {
	
	int i, j;

	i = j = 0;

	for ( i ; s[i] != '\0'; i++ ) ;

	while ( ( s[i++] = t[j++] ) != '\0' ) ;

}

void
main (int argc, char *argv[]){

	char s[MAXLINE] = "FIRST PART, ";

	mstrcat(s, "SECOND PART!!\n");

	printf("%s", s);

}

