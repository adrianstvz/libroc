#include <stdio.h>
#include <string.h>

#define MAXLINES 5000

char *lineptr[MAXLINES];

int readlines (char **, int) ;
void writelines (char **, int) ;

#define MAXLEN 1000
#define ALLOCSIZE MAXLEN * MAXLINES

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;

int mgetline (char *, int) ;
char *alloc (int);
void qsort (char **, int, int) ;
void swap (char **, int, int) ;

int 
readlines ( char *lineptr[], int maxlines) {

	int len, nlines;
	char *p, line[MAXLEN];

	nlines = 0;
	while (( len = mgetline(line, MAXLEN) ) > 0 ) {
		if (nlines >= maxlines || (p = alloc(len)) == NULL)
			return -1;
		else {
			line[len-1] = '\0';
			strcpy(p, line);
			lineptr[nlines++] = p;
		}
	}
	return nlines;
}

void
writelines (char *lineptr[], int nlines) {
	
	while ( nlines-- > 0 )
		printf("%s\n", *lineptr++ );

}

int 
mgetline (char *s, int n) {

	char c;
	char *p;

	p = s;

	while ( n-- > 0 && ( c = getchar() ) != EOF && c != '\n' ) *s++ = c;

	if ( c == '\n' ) *s++ = c;

	*s = '\0';

	return s - p;

}

char *
alloc (int n) {

	if ( allocbuf + ALLOCSIZE - allocp >= n ) {
		allocp += n;
		return allocp - n;
	} else {
		return 0;
	}

}

void
qsort (char *v[], int l, int r) {

	int i, last;
	
	if ( l >= r ) return;

	swap( v, l, (l+r)/2);

	last = l;

	for ( i = l+1; i <= r; i++ ) {
		if (strcmp(v[i], v[l]) < 0 )
			swap(v, ++last, i);
	}

	swap(v, l, last);
	qsort(v, l, last-1);
	qsort(v, last+1, r);

}

void
swap (char *v[], int i, int j) {

	char *tmp;

	tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
	
}

void
main (int argc, char *argv[]) {

	int nlines;

	if (( nlines = readlines(lineptr, MAXLINES)) >= 0 ) {
		qsort(lineptr,0, nlines-1);
		writelines(lineptr, nlines);
	} else {
		printf("error: input too big to sort\n");
	}

}

