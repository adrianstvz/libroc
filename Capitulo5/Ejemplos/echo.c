#include <stdio.h>

/* Version 1
void
main (int argc, char *argv[]) {

	int i;

	for ( i=0; i < argc ; i++ ) 
		printf("%s%s", argv[i], ( i < argc - 1 ) ? " " : "");

	printf("\n");
	return 0;

}
*/

/* Version 2 */
void
main (int argc, char *argv[]) {

	while ( --argc > 0 ) 
		//printf("%s%s", *(++argv), ( argc > 1 ) ? " " : "");
		printf( (argc > 1 ) ? "%s " : "%s", *(++argv) );

	printf("\n");

}
