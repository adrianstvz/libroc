#include <stdio.h>

void astrcpy (char *, char *) ;
void pstrcpy (char *, char *) ;
void pstrcpy2 (char *, char *) ;

void 
astrcpy ( char *s, char *t) {
// Copy s to t

	int i;
	for ( i = 0; s[i] != '\0'; i++ ) 
		t[i] = s[i];

}

void 
pstrcpy ( char *s, char *t) {
// Copy s to t

	while ( (*t = *s) != '\0' ) {
		t++, s++;
	}

}

void 
pstrcpy2 ( char *s, char *t) {
// Copy s to t

	while ( (*t++ = *s++) != '\0' );

}

void 
pstrcpy3 ( char *s, char *t) {
// Copy s to t

	while ( *t++ = *s++ ) ;

}


void main(){

	char amessage[] = "Hello world"; 	// Array
	char *pmessage = "Hello World!!";	// Pointer

	char acopy[1000];
	char *pcopy;

	astrcpy(amessage, acopy);
	pstrcpy(pmessage, pcopy);

	printf("A %s\n", acopy);
	printf("P %s\n", pcopy);

	pstrcpy2(amessage, acopy);
	pstrcpy2(pmessage, pcopy);

	printf("A %s\n", acopy);
	printf("P %s\n", pcopy);

	pstrcpy3(amessage, acopy);
	pstrcpy3(pmessage, pcopy);

	printf("A %s\n", acopy);
	printf("P %s\n", pcopy);

}
