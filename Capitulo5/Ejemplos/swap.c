#include <stdio.h>

void swap (int *, int *) ;

void
swap (int *x, int *y) {

	int tmp;

	tmp = *x;
	*x = *y;
	*y = tmp;

}

void
main (int argc, char *argv[]) {

	int x, y;

	x = 10;
	y = 99;

	printf("x: %d, y: %d\n", x, y);
	swap(&x, &y);
	printf("x: %d, y: %d\n", x, y);

}

