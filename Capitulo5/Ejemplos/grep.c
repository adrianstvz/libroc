#include <stdio.h>
#include <string.h>
#define MAXLINE 1000

int mgetline (char *, int) ;

int mgetline (char *line, int max) {

	char c;
	int i;

	for ( i = 0; i < max && ( c = getchar() ) != EOF && c != '\n'; i++ )
		line[i] = c;

	if ( c == '\n' ) line[i++] = c;

	line[i] = '\0';

	return i;
}

void
main (int argc, char *argv[]) {

	char line[MAXLINE];
	long linen = 0;
	int c, except = 0, number = 0, found = 0;

	while ( --argc > 0 && (*++argv)[0] == '-' ) {
		while ( c = *++argv[0]) {
				switch(c) {
					case 'x':
						except = 1;
						break;
					case 'n':
						number = 1;
						break;
					default:
						printf("Unsuported option: %c\n", c);
						argc = 0;
						break;
				}
		}
	}

	if ( argc != 1 ) printf("Usage: grep -x -n pattern\n");
	else {
		while ( mgetline(line, MAXLINE) > 0 ) {
			linen++;

			if ( ( strstr(line, *argv) != NULL ) != except ) {
				if (number) printf("%ld: ", linen);
				printf("%s", line);
				found++;
			}
		}
	}

}

