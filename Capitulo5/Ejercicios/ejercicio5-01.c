#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define SIZE 20

int getch (void) ;
void ungetch (char) ;
int getint (int *) ;

int
getint (int *n) {

	char c, tmp;
	int sign;

	while ( isspace(c = getch()) ) ;

	if ( !isdigit(c) && c != EOF && c != '+' && c != '-' ) {
		// Not a number
		ungetch(c);
		return 0;
	}

	sign = ( c == '-' ) ? -1 : 1;

	if ( c == '+' || c == '-' ) {
		tmp = c;
		if ( !isdigit(c = getch()) ) {
			// If c is not digit, ungetch c and previous and return
			ungetch(tmp);
			ungetch(c);
			return 0;
		}
	}

	for ( *n = 0; isdigit(c); c = getch() )
			*n = *n * 10 + ( c - '0' );
	*n *= sign;

	if ( c != EOF )
		ungetch(c);

	return c;
}

void
main (int argc, char *argv[]) {

	int n, i, r, array[SIZE];

	for ( n = 0; n<SIZE && ( r = getint(&array[n]) ) != EOF && r > 0; n++ ) ;

	for ( i = 0; i < n; i++ ) printf("%d%c", array[i], (i == (n-1))?'\n':',');

}

