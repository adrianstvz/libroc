#include <stdio.h>
#include <stdlib.h>

#define ERROR { printf("Error on line: %d\n", __LINE__); return; }

#define MAXLINE 1000
#define MAXSTOPS 100
#define TS 4

#define DETAB 1
#define ENTAB 2

void detab (int, int);
void entab (int, int);


void
detab (int start, int nstops) {
/* This function replaces tabs with N blanks */

	char c;
	int i, col;

	i = col = 0;
	
	while( ( c = getchar() ) != EOF ) {
		switch ( c ) {
			case '\t':
				
				if ( col  >= start ) {
					for ( i = col % nstops; i < nstops; i++ ) 
						putchar(' ');
				} else {
					putchar('\t');
				}

				col+=nstops;
				break;
			case '\n':
				putchar(c);
				col = 0;
				break;
			default:
				putchar(c);
				col++;
				break;
		}
	}

}

void
entab (int start, int nstops) {
/* This function replaces N spaces with a tab */

	char c;
	int i, col, blanks;

	i = col = blanks = 0;
	
	while( ( c = getchar() ) != EOF ) {
		switch ( c ) {
			case ' ':
				blanks++;

				// Only add tabs if inside of range
				if ( ( blanks + col ) > start ) {
					if ( ( blanks + ( col % nstops )) >= nstops ) {
						putchar('\t');
						col += blanks;		// col gets value of blanks
						blanks = 0;			// restart blanks
					}
				} else {
					putchar(' ');
					blanks = 0;
					col++;
				}

				break;
			case '\n':
				putchar(c);
				col = 0;
				break;
			default:

				col += blanks+1;
				for ( ; blanks > 0 ; blanks-- ) putchar(' ');	

				putchar(c);

				break;
		}
	}

}


void
main (int argc, char *argv[]){

	int i, n, prev;
	char op = DETAB;

	char line[MAXLINE];
	int nstops = TS,
		col_start = 0;

	if ( argc-- > MAXSTOPS ) ERROR;

	prev = 0;

	if ( argc-- > 0 ) {
		switch ( (*++argv)[0]){
			case 'd': 
				op=DETAB;
				break;
			case 'e':
				op=ENTAB;
				break;
			default: 
				argv--;
				argc++;
				break;
		}
	}

	for ( i = 0; i < argc; i++ ) {
		switch ( (*++argv)[0] ) {
			case '-': 
				if ( ( col_start = atoi(*argv + 1) ) <= 0 ) {
					printf("Invalid argument");
					return;
				}
				break;
			case '+':
				if ( ( nstops = atoi(*argv + 1) ) <= 0 ) {
					printf("Invalid argument");
					return;
				}
				break;
			default:
				printf("Unknown operation\n");
				return;
		}

	}

	switch (op) {
		case DETAB:
			detab(col_start, nstops);
			break;
		case ENTAB:
			entab(col_start, nstops);
			break;
		default:
			ERROR;
			break;
	}
}
