#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "quicksort.h"
#include "lines.h"

#define VOIDCOMP int (*) (void*, void*)

int compare(char *, char *);
int numcmp(char *, char *);
int numcmpr(char *, char *);
int mstrcmp(char *, char *);
int mstrcmpr(char *, char *);

int (*comp) (char *, char*);
char *lineptr[MAXLINES];

int directory = 0;
int fold = 0;
int numeric = 0;
int reverse = 0;

int
mstrcmp (char *s1, char*s2) {

	if ( directory ) { // Skip non blanks or alphanumeric
		while(!isdigit(*s1) && !isalpha(*s1) && !isspace(*s1) && *s1 != '\0') s1++;
		while(!isdigit(*s2) && !isalpha(*s2) && !isspace(*s2) && *s2 != '\0') s2++;
	}
	while ( fold ? (tolower(*s1) == tolower(*s2)) : (*s1 == *s2) ) {
		if ( *s1 == '\0' ) return 0;
		*s1++;
		*s2++;
		if ( directory ) { // Skip non blanks or alphanumeric
			while(!isdigit(*s1) && !isalpha(*s1) && !isspace(*s1) && *s1 != '\0') s1++;
			while(!isdigit(*s2) && !isalpha(*s2) && !isspace(*s2) && *s2 != '\0') s2++;
		}
	}

	return fold ? (tolower(*s1) - tolower(*s2)) : ( *s1 - *s2 );

}

int
mstrcmpr (char *s1, char *s2) {
	
	return mstrcmp(s2, s1);

}

int
numcmp (char *s1, char*s2) {

	double v1, v2;

	v1 = atof(s1);
	v2 = atof(s2);

	if ( v1 < v2 ) return -1;
	else if ( v1 > v2) return 1;
	else return 0;

}

int
numcmpr (char *s1, char *s2) {
	
	return numcmp(s2, s1);

}

int
compare (char *s1, char *s2) {

	return (*comp)(s1, s2);
}

void
main (int argc, char *argv[]) {

	int nlines;

	while ( argc-- > 1 ) {
		argv++;
		if ( *argv[0] == '-' ) {
			while ( *++(*argv) != '\0' )
				switch (**argv) {
					case 'd':
						directory = 1;
						break;
					case 'f':
						fold = 1;
						break;
					case 'n': 
						numeric = 1;
						break;
					case 'r': 
						reverse = 1;
						break;
					default:
						printf("Uknown option\n");
						return;
				}
		}
	}

	if ( numeric && reverse ) comp = numcmpr;
	else if ( numeric ) comp = numcmp;
	else if ( reverse ) comp = mstrcmpr;
	else comp = mstrcmp;

	if (( nlines = readlines(lineptr, MAXLINES)) < 0 ) {
		printf("error: input too big to sort\n");
		return;
	}
	
	// Create fields
	quicksort( (void **)lineptr,0, nlines-1, (VOIDCOMP)compare);
	writelines(lineptr, nlines);

}

