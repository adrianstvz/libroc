#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define NUMBER 0
#define MAXOP 100
#define MAXVAL 100

int getop (char *);
double pop (void) ;
void push (double) ;

static int sp = 0;
static double stack[MAXVAL];

double
pop (void) {

	if ( sp > 0 ) return stack[--sp];
	else {
		printf("error: stack is empty\n");
		return 0.0;
	}

}

void
push (double x) {

	if ( sp < MAXVAL ) stack[sp++] = x;
	else printf("error: stack is full");

}

int getop (char *s) {

	for ( ; *s == ' ' || *s == '\t'; s++ );

	if (!isdigit(*s) && *s != '.' ) return *s; // Not a number

	if ( isdigit(*s) )
		while ( isdigit(*++s) );

	if ( *s == '.' )
		while ( isdigit(*++s) );

	return NUMBER;
}


void
main (int argc, char *argv[]) {

	int i;
	double op;
	char s[MAXOP];

	while ( argc-- > 0 ) {
		strcpy(s,*argv++);
		switch ( i = getop(s) ) {
			case NUMBER:
				push(atof(s));
				break;
			case '+':
				push(pop() + pop());
				break;
			case '*':
				push(pop() * pop());
				break;
			case '-':
				op = pop();
				push(pop() - op);
				break;
			case '/':
				op = pop();
				if ( op != 0.0 ) 
					push(pop() / op);
				else
					printf("error: illegal expresion, division by zero\n");
				break;
			default:
				printf("error: unknown command '%s'\n", s);
				break;
		}

	}

	printf("\t%.8g\n", pop());

}

