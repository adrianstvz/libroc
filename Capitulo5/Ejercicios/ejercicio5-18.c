#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../Ejemplos/getch.h"

#define MAXTOKEN 100

enum { NAME, PARENS, BRACKETS };
enum { OK = 0, ERROR };

int dcl (void) ;
int dirdcl (void) ;
void nextline (void) ;

int gettoken(void) ;

int tokentype;
char token[MAXTOKEN];
char name[MAXTOKEN];
char datatype[MAXTOKEN];
char out[1000];

void
nextline (void) {
	 
	int c;

	while ( ( c = getch() ) != EOF && c != '\n' ) ;

	if ( c == EOF ) ungetch(c);

}

int
dcl (void) {

	int ns;

	for (ns = 0; gettoken() == '*'; ) ns++;

	if ( dirdcl() != OK ) return ERROR;

	while (ns-- > 0)
		strcat(out, " pointer to");
	return OK;

}

int
dirdcl (void) {

	int type;
	
	if (tokentype =='(') {
		if ( dcl() != OK ) return ERROR;
		if (tokentype != ')') {
			printf("error: missing )\n");
			return ERROR;
		}

	} else if (tokentype == NAME) {
		strcpy(name, token);
	} else {
		printf("error: expected name or (dcl)\n");
		return ERROR;	
	}

	while (( type = gettoken() ) == PARENS || type == BRACKETS ) {
		if (type == PARENS) {
			strcat(out, " function returning");
		} else {
			strcat(out, " array");
			strcat(out, token);
			strcat(out, " of");
		}
	}

	return OK;
}

int
gettoken (void) {

	int c;
	char *p = token;

	// Skip blanks
	while ( ( c = getch() ) == ' ' || c == '\t' ) ;

	if ( c == '(') {
		if ( ( c = getch() ) == ')') {
			strcpy(token, "()");
			return tokentype = PARENS;
		} else {
			ungetch(c);
			return tokentype = '(';
		}
	} else if ( c == '[' ) {
		for ( *p++ = c; (*p++ = getch()) != ']'; );
		*p = '\0';
		return tokentype = BRACKETS;

	} else if ( isalpha(c) ) {
		for ( *p++ = c; isalnum( c = getch() ); )
			*p++ = c;
		ungetch(c);
		*p = '\0';
		return tokentype = NAME;
	} else {
		return tokentype = c;
	}


}

/* MAIN */

int
main (int argc, char *argv[]) {

	while ( gettoken() != EOF ) {
		strcpy(datatype, token);
		out[0] = '\0';
		if ( dcl() != OK ) // If error skip line
			nextline();
		else if (tokentype != '\n') { // If error skip line
			printf("syntax error\n");
			nextline();
		} else {
			printf("%s: %s %s\n", name, out, datatype);
		}
	}

	return 0;

}

