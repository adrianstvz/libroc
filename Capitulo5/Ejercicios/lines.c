#include <stdio.h>
#include <string.h>
#include "alloc.h"
#include "lines.h"

int 
mgetline (char *s, int n) {

	char c;
	char *p;

	p = s;

	while ( n-- > 0 && ( c = getchar() ) != EOF && c != '\n' )
		*s++ = c;

	if ( c == '\n' )
		*s++ = c;

	*s = '\0';

	return s - p;

}

int 
readlines ( char *lineptr[], int maxlines) {

	int len, nlines;
	char *p, line[MAXLEN];

	nlines = 0;
	while (( len = mgetline(line, MAXLEN) ) > 0 ) {
		if (nlines >= maxlines || (p = alloc(len+1)) == NULL)
			return -1;
		
		line[len] = '\0';
		strcpy(p, line);
		lineptr[nlines++] = p;

	}
	return nlines;
}

void
writelines (char *lineptr[], int nlines) {
	
	while ( nlines-- > 0 )
		printf("%s", *lineptr++ );

}
