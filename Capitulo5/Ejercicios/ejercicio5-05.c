#include <stdio.h>
#include <string.h>

#define MAXLINE 1000

void pstrncpy (char *, char*, int) ;
void pstrncat (char *, char*, int) ;
int pstrncmp (char *, char*, int) ;

void
pstrncpy ( char *s, char *t, int n ) {

	while ( n-- > 0 && ( *s++ = *t++ ) != '\0' ) ;

	while (*s++ != '\0');

}

void
pstrncat ( char *s, char *t, int n ) {

	while ( *s++ != '\0' ) ;

	s--;

	for ( ; n-- > 0 && ( *s = *t ) != '\0'; s++, t++ ) ;

	*s = '\0';

}

int
pstrncmp ( char *s, char *t, int n ) {

	for ( ; *s == *t ; s++, t++ )
		if ( *s == '\0' || --n <= 0 ) return 0;

	return *s - *t;

}

void
main (int argc, char *argv[]) {

	char s[MAXLINE] = "ORNITHOLOGIST";
	char t[MAXLINE] = "NITROGEN";
	char u[MAXLINE] = "WORD";
	int n;

	printf("%s - %s\n", s, t);
	pstrncpy(s,t,n=2);
	printf("(%d) %s\n",n, s);
	pstrncpy(s,t,n=5);
	printf("(%d) %s\n",n, s);
	pstrncpy(s,t,n=8);
	printf("(%d) %s\n",n, s);
	pstrncpy(s,t,n=9);
	printf("(%d) %s\n",n, s);

	printf("%s - %s\n", s, u);
	pstrncat(s,u,n=2);
	printf("(%d) %s\n",n, s);
	pstrncat(s,u,n=4);
	printf("(%d) %s\n",n, s);
	pstrncat(s,u,n=6);
	printf("(%d) %s\n",n, s);

	n=2;
	printf("%s ==(%d) %s : %s\n", s, n, t, pstrncmp(s,t,n) ? "FALSE" : "TRUE");
	n=4;
	printf("%s ==(%d) %s : %s\n", s, n, t, pstrncmp(s,t,n) ? "FALSE" : "TRUE");
	n=8;
	printf("%s ==(%d) %s : %s\n", s, n, t, pstrncmp(s,t,n) ? "FALSE" : "TRUE");
	n=12;
	printf("%s ==(%d) %s : %s\n", s, n, t, pstrncmp(s,t,n) ? "FALSE" : "TRUE");
	

}

