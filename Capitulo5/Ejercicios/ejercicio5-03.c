#include <stdio.h>

#define MAXLINE 1000

void pstrcat (char *, char *);

void
pstrcat (char *s, char *t) {
	
	while ( *s++ != '\0' ) ;

	s--; 

	while ( ( *s = *t ) != '\0' )
		s++, t++ ;

}

void
main (int argc, char *argv[]){

	char s[MAXLINE] = "FIRST PART, ";

	pstrcat(s, "SECOND PART!!\n");

	printf("%s", s);

}

