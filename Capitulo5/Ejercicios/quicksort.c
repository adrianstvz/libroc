#include "quicksort.h"

/* Quicksort */
void
quicksort (void *v[], int l, int r, int (*comp)(void *, void *)) {

	int i, last;
	
	if ( l >= r ) return;

	swap( v, l, (l+r)/2);

	last = l;

	for ( i = l+1; i <= r; i++ ) {
		if ((*comp)(v[l], v[i]) > 0 )
			swap(v, ++last, i);
	}

	swap(v, l, last);
	quicksort(v, l, last-1, comp);
	quicksort(v, last+1, r, comp);

}

/* Swap */
void
swap (void **v, int i, int j) {

	void *tmp;

	tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
	
}


