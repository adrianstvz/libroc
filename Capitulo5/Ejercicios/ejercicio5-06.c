#include <stdio.h>
#include <ctype.h>

#define MAXLINE 1000

int mgetline (char *, int) ;
int atoi (char *) ;
float atof (char *) ;
void itoa (char *, int) ;

void reverse (char *) ;
int strindex (char *, char *) ;

int
atoi (char * s) {

	int n = 0;
	int sign;

	while ( isblank(*s++) ) ;
	s--;

	sign = ( *s == '-' ) ? -1 : 1;

	if ( sign < 0 ) s++;

	for ( ; isdigit(*s); s++ )
		n = n*10 + *s - '0';

	return sign * n;

}

void
itoa (char *s, int n) {

	int sign;

	char *p;

	p = s;

	sign = ( n < 0 ) ? -1 : 1 ;

	n*=sign;

	for ( ; n > 0 ; n /= 10 ) *s++ = '0' + n % 10;

	if ( sign < 0 ) *s++='-';		// Add - if negative
	*s = '\0';

	reverse(p);

}

int 
mgetline (char *s, int n) {

	char c;
	char *p;

	p = s; // Save starting position

	while ( n-- > 0 && (c = getchar()) != EOF && c != '\n' ) *s++ = c ;

	if ( c == '\n' ) *s++ = c;
	
	*s = '\0';

	return s - p;

}

void
reverse (char *s) {

	char *p;
	char tmp;

	p = s;

	while (*s++ != '\0' ) ;

	s-=2;	// We go back to position before '\0'

	for ( ; s > p ; s--, p++ ) {
		tmp = *p;
		*p = *s;
		*s = tmp;
	}

}

int 
strindex (char *s, char *t) {

	char *p, *u, *v;

	p = s;

	for ( ; *s != '\0' ; s++ ) {

		for ( u = s, v = t ; *v != '\0' && *v == *u; v++, u++ ) ;

		if ( *v == '\0' ) return s - p;
	}

	return -1; 

}

void
main (int argc, char *argv[]) {

	char s[MAXLINE];
	char match[MAXLINE] = "hello";
	int i;

	while ( mgetline(s, MAXLINE) ) {
		if ( strindex(s, match) >= 0 ) {
			printf("%s", s);
			reverse(s);
			printf("----%s\n", s);
		} else {
			printf("%d -", i = atoi(s));
			itoa(s,i); 
			printf("> %s\n", s);
		}
	}

}

