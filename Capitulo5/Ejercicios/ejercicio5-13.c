#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERROR { printf("Error in line %d\n", __LINE__); return; }

#define MAXLINE 1000
#define NL 10

static char *line_buffer[MAXLINE] ;

int mgetline (char *, int) ;


// FUNCTIONS
int
mgetline (char *s, int lim) {

	char c;
	char *ps = s;
	
	while (--lim && ( c = getchar() ) != EOF && c != '\n' ) 
		*s++ = c;

	if ( c == '\n' ) *s++ = c;

	*s = '\0';

	return s - ps;

}

void
main (int argc, char *argv[]) {

	int i, count, len,
		n = NL,
		total = 0;

	char line[MAXLINE];
	char *p;

	if ( argc > 1){
		if ( (*++argv)[0] == '-' && (n = atoi(++(*argv)))) ;
		else {
			printf("Invalid argument found"); 
			return;
		}

		if ( n > MAXLINE || n < 0 ) {
			printf("Invalid number of lines");
			return;
		}
	}

	while ( ( len = mgetline(line, sizeof line) ) > 0 ) {

		i = total++ % n;
		line[len-1] = '\0'; // Remove newline

		free(line_buffer[i]);
		if (( p = malloc(len) ) == NULL) ERROR;
		strcpy(p, line);
		line_buffer[i] = p;

	}

	if ( total < n ) i = 0;
	else i = total % n;

	for ( count = 0; count < n && count < total; count++ ) {
		printf("%s\n", line_buffer[i]);
		i = ( i + 1 ) % n;
	}

}

