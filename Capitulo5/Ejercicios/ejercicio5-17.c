#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "quicksort.h"
#include "lines.h"

#define VOIDCOMP int (*) (void *, void *)

#define ODIR 1
#define OFLD 2
#define ONUM 4
#define OREV 8

#define MAXFIELDS 100

int compare(char **, char **) ;
int numcmp(char *, char *) ;
int numcmpr(char *, char *) ;
int mstrcmp(char *, char *) ;
int mstrcmpr(char *, char *) ;

void dirsort (char *) ;
void fold (char *) ;
char *line (char **) ;
char **prepare_data (char *) ;

int (*comp) (char *, char*);

int (*compares[MAXFIELDS])(char*, char*);
int options[MAXFIELDS] ;
int fieldsid[MAXFIELDS] ;
int nfields = 0;
int lastfield = 0;

char *lineptr[MAXLINES];


void 
dirsort (char *s) {

	char c, *d;

	d = s;

	while ( c = *s++ ) {
		if ( isdigit(c) || isalpha(c) || isspace(c) )
			*d++ = c;
	}

	*d = '\0';

}

void
fold (char *s) {

	while (*s = tolower(*s))
		s++;

}

int
mstrcmp (char *s1, char*s2) {
	//printf("MSTRCMP\n");

	while ( *s1 == *s2 ) {
		if ( *s1 == '\0' ) return 0;
		*s1++;
		*s2++;
	}

	return *s1 - *s2 ;

}

int
mstrcmpr (char *s1, char *s2) {
	
	//printf("REV ");
	return mstrcmp(s2, s1);

}

int
numcmp (char *s1, char*s2) {

	//printf("NUMCMP\n");
	double v1, v2;

	v1 = atof(s1);
	v2 = atof(s2);

	if ( v1 < v2 ) return -1;
	else if ( v1 > v2) return 1;
	else return 0;

}

int
numcmpr (char *s1, char *s2) {
	
	//printf("REV ");
	return numcmp(s2, s1);

}

int
compare (char **s1, char **s2) {

	int i, result = 0;

	// Apply the correct comp function to each field
	for ( i = 0; i < nfields && result == 0; i++ )
		result = (*compares[i])(s1[i], s2[i]);

	return result;

}

char **
prepare_data (char *line) {
	
	char *fieldspace = malloc( (strlen(line) ) * sizeof(char));
	char **tmpfields = malloc( (nfields + 1) * sizeof(line) );
	char **data = malloc( (nfields + 2) * sizeof(line) );
	
	char *field, *c;
	int i;

	// Copy line into fieldspace
	strcpy(fieldspace, line);

	field = fieldspace;
	i = 0;
	for ( c = fieldspace; *c; c++ ) {
		// Create new field on tabs and newlines
		if ( *c == '\t' || *c == '\n' ) {
			*c = '\0';

			// Add field to list if previous to last and load next one
			if ( i <= lastfield + 1)
				tmpfields[i] = field;

			field = c + 1;
			i++;
		}
	}

	// Apply changes to each field
	for ( i = 0; i < nfields; i++ ) {

		field = tmpfields[fieldsid[i]] ;

		if ( options[i] & OFLD ) fold(field);

		if ( options[i] & ODIR ) dirsort(field);

		data[i] = field;

	}
	data[nfields] = line;	// Store the line
	data[nfields+1] = fieldspace;	// And the fields

	free(tmpfields);

	/**
	for ( i = 0; i < nfields; i++ ) {
		printf("%s, ", data[i]);
	}
	printf("%s", data[i]);
	*/

	return data;

}

void
print_fields (void ) {

	printf("Printing a total of %d fields\n", nfields);
	for ( int i = 0; i < nfields; i++ ) {
		printf("Field: %d, ", fieldsid[i]);
		printf("Options: %c%c%c%c, ",	options[i] & OREV ? 'r' : '-',
									options[i] & ONUM ? 'n' : '-',
									options[i] & OFLD ? 'f' : '-',
									options[i] & ODIR ? 'd' : '-' );
		printf("\n");
	}
}

void
main (int argc, char *argv[]) {

	int i, n, nlines;

	char ***linedata;

	// Read Arguments
	for ( i = 0; i < argc - 1; i++ ) {
		options[i] = 0;
		fieldsid[i] = 0;
		compares[i] = mstrcmp;

		argv++;
		
		if ( **argv == '-' && isdigit(*++*argv) ) {
			n = atoi(*argv) - 1;
			while (*++*argv)	{
				switch (**argv) {
					case 'd':
						options[nfields] |= ODIR;
						break;
					case 'f':
						options[nfields] |= OFLD;
						break;
					case 'n': 
						options[nfields] |= ONUM;
						break;
					case 'r': 
						options[nfields] |= OREV;
						break;
				}	// SWITCH
			}	// WHILE

			lastfield = ( n > lastfield ) ? n : lastfield;

			// Select the sort function
			if ( ( options[nfields] & ONUM ) && ( options[nfields] & OREV ) ) comp = numcmpr;
			else if ( options[nfields] & ONUM ) comp = numcmp;
			else if ( options[nfields] & OREV ) comp = mstrcmpr;
			else comp = mstrcmp;
			
			// Add the field id
			fieldsid[nfields] = n;

			// Add the sort function to the list
			compares[nfields] = comp;

			nfields++;

		}	// IF
	}	// FOR

	//print_fields();

	// Read Lines
	if (( nlines = readlines(lineptr, MAXLINES)) < 0 ) {
		printf("error: input too big to sort\n");
		return;
	}

	// Create fields
	linedata = malloc(nlines * sizeof(char **));
	for ( i = 0; i < nlines; i++ ) {
		linedata[i] = prepare_data(lineptr[i]);
	}

	// Sort lines
	quicksort( (void **)linedata, 0, nlines-1, (VOIDCOMP) compare);

	// Recover fields
	for ( i = 0; i < nlines; i++ )
		lineptr[i] = linedata[i][nfields];

	writelines(lineptr, nlines);

	for ( i = 0; i < nlines; i++ ) {
		free( linedata[i][nfields+1] );
	}

	free(linedata);

}

