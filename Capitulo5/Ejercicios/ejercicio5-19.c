#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../Ejemplos/getch.h"

#define MAXTOKEN 100

enum { NAME, PARENS, BRACKETS };
enum { OUT, IN };

int gettoken(void) ;

int tokentype;
char token[MAXTOKEN];
char name[MAXTOKEN];
char datatype[MAXTOKEN];
char out[1000];

int
gettoken (void) {

	int c;
	char *p = token;

	while ( ( c = getch() ) == ' ' || c == '\t' ) ;

	if ( c == '(') {
		if ( ( c = getch() ) == ')') {
			strcpy(token, "()");
			return tokentype = PARENS;
		} else {
			ungetch(c);
			return tokentype = '(';
		}
	} else if ( c == '[' ) {
		for ( *p++ = c; (*p++ = getch()) != ']'; );
		*p = '\0';
		return tokentype = BRACKETS;

	} else if ( isalpha(c) ) {
		for ( *p++ = c; isalnum( c = getch() ); )
			*p++ = c;
		ungetch(c);
		*p = '\0';
		return tokentype = NAME;
	} else 
		return tokentype = c;


}

/* MAIN */

void
main (int argc, char *argv[]) {

	int type, state = OUT;
	char temp[MAXTOKEN];

	while ( gettoken() != EOF ) {
		strcpy(out, token);
		while ( ( type = gettoken() ) != '\n' ) {
			if ( type == PARENS  || type == BRACKETS ) {
				if ( state == IN ) {
					sprintf(temp, "(%s)", out);	// If declaration comes after pointer
					strcpy(out, temp);			// Close parentheses
				}	
				strcat(out, token);
				state = OUT;
			} else if (type == '*') {
				sprintf(temp, "*%s", out);
				state = IN;	// State IN after finding a pointer
				strcpy(out, temp);
			} else if ( type == NAME ) {
				sprintf(temp, "%s %s", token, out);
				strcpy(out, temp);
			} else {
				printf("invalid input as %s\n", token);
			}
		} 
		printf("%s\n", out);
	}

}

