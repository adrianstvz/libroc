#include <stdio.h>

#define MAXLINE 1000

int pstrend (char *, char *);

int
pstrend (char *s, char *t) {
	
	char *start = t;

	while ( *s++ != '\0' ) ;

	while ( *t++ != '\0' ) ;

	while ( *s == *t && t >= start ) s--, t-- ;

	return t < start;
}

void
main (int argc, char *argv[]){

	char s[MAXLINE] = "TEST STRING";
	char t[MAXLINE] = "STRING";

	printf("%s, %s : %s\n", s, t, (pstrend(s, t)) ? "TRUE" : "FALSE" );

	char u[MAXLINE] = "SSTRING";
	printf("%s, %s : %s\n", s, u, (pstrend(s, u)) ? "TRUE" : "FALSE" );

}

