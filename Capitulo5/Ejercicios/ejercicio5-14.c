#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXLINES 5000
#define MAXLEN 1000
#define ALLOCSIZE MAXLEN * MAXLINES

char *lineptr[MAXLINES];
static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;

int readlines (char **, int) ;
void writelines (char **, int, int) ;

int mgetline (char *, int) ;
char *alloc (int);
void swap (void **, int, int) ;
void mqsort (void **, int, int, int (*)(void *, void*)) ;
int numcmp(char *, char *);

int 
readlines ( char *lineptr[], int maxlines) {

	int len, nlines;
	char *p, line[MAXLEN];

	nlines = 0;
	while (( len = mgetline(line, MAXLEN) ) > 0 ) {
		if (nlines >= maxlines || (p = alloc(len)) == NULL)
			return -1;
		else {
			line[len-1] = '\0';
			strcpy(p, line);
			lineptr[nlines++] = p;
		}
	}
	return nlines;
}

void
writelines (char *lineptr[], int nlines, int reverse) {
	
	while ( nlines-- > 0 )
		printf("%s\n", reverse ? lineptr[nlines] : *lineptr++ );
		//printf("%s\n", *lineptr++ );

}

int 
mgetline (char *s, int n) {

	char c;
	char *p;

	p = s;

	while ( n-- > 0 && ( c = getchar() ) != EOF && c != '\n' ) *s++ = c;

	if ( c == '\n' ) *s++ = c;

	*s = '\0';

	return s - p;

}

char *
alloc (int n) {

	if ( allocbuf + ALLOCSIZE - allocp >= n ) {
		allocp += n;
		return allocp - n;
	} else {
		return 0;
	}

}

void
mqsort (void *v[], int l, int r, int (*comp)(void *, void *)) {

	int i, last;
	
	if ( l >= r ) return;

	swap( v, l, (l+r)/2);

	last = l;

	for ( i = l+1; i <= r; i++ ) {
		if ((*comp)(v[i], v[l]) < 0 )
			swap(v, ++last, i);
	}

	swap(v, l, last);
	mqsort(v, l, last-1, comp);
	mqsort(v, last+1, r, comp);

}


int
numcmp (char *s1, char*s2) {

	double v1, v2;

	v1 = atof(s1);
	v2 = atof(s2);

	if ( v1 < v2 ) return -1;
	else if ( v1 > v2) return 1;
	else return 0;

}


void
swap (void **v, int i, int j) {

	void *tmp;

	tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
	
}

void
main (int argc, char *argv[]) {

	int nlines;
	int numeric = 0,
		reverse = 0;

	while ( argc-- > 1 ) {
		argv++;
		if ( *argv[0] == '-' ) {
			while ( *++(*argv) != '\0' )
				switch (**argv) {
					case 'n': 
						numeric = 1;
						break;
					case 'r': 
						reverse = 1;
						break;
					default:
						printf("Uknown option\n");
						return;
				}
		}
	}

	if (( nlines = readlines(lineptr, MAXLINES)) >= 0 ) {
		mqsort( (void **)lineptr,0, nlines-1, 
				(int (*)(void*, void*))( numeric ? numcmp : strcmp) );
		writelines(lineptr, nlines, reverse);
	} else {
		printf("error: input too big to sort\n");
	}

}

