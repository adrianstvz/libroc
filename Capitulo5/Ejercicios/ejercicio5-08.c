#include <stdio.h>

static char daytab[2][13] = {
	{0, 31, 28, 31,30, 31, 30, 31, 31, 30, 31, 30, 31} ,
	{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int day_of_year (int, int, int) ;
int month_day (int, int, int *, int *) ;

int
day_of_year (int year, int month, int day) {

	int i, leap;

	if ( year < 0 || month < 1 || month > 12 || day < 1 ) return -1;	// Check valid values

	leap = year % 4 == 0 && year % 100 || year % 400 == 0;

	if ( day > daytab[leap][month] ) return -1;	// Check month has enough days

	for ( i = 1; i < month; i++ ) {
		day += daytab[leap][i];
	}

	return day;
}

int
month_day (int year, int yearday, int *pmonth, int *pday) {

	int i, leap;

	if ( year < 0 || yearday < 1 ) return -1;

	leap = year % 4 == 0 && year % 100 || year % 400 == 0;

	for ( i = 1; ( yearday > daytab[leap][i] ) && ( i < 12 ) ; i++)
			yearday -= daytab[leap][i];

	if ( yearday > daytab[leap][i] ) return -1;

	*pmonth = i;
	*pday = yearday;

	return 0;

}

void
main (int argc, char *argv[]) {

	int year, month, day, yearday;
		
	for (year = 1970; year <= 2000; year++) {
		for (yearday = 1; yearday <= 366; yearday++) {
			if (month_day(year, yearday, &month, &day) < 0) {
				printf("month_day failed: %d %d\n", year, yearday);
			} else if (day_of_year(year, month, day) != yearday) {
				printf("bad result: %d %d\n", year, yearday);
				printf("month = %d, day = %d\n", month, day);
			}
		}
	}

}

