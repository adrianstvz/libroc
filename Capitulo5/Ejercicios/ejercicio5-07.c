#include <stdio.h>
#include <string.h>

#define MAXLINES 5000
#define MAXLEN 1000
#define MAXSIZE MAXLEN * MAXLINES

char *lineptr[MAXLINES];
char linestore[MAXLEN];

int readlines (char **, char *, int) ;
void writelines (char **, int) ;

int mgetline (char *, int) ;
void qsort (char **, int, int) ;
void swap (char **, int, int) ;

int 
readlines ( char *lineptr[], char *linestore, int maxlines) {

	int len, nlines;
	char *p, *linestop, line[MAXLEN];

	p = linestore;
	linestop = linestore + MAXSIZE;

	nlines = 0;
	while (( len = mgetline(line, MAXLEN) ) > 0 ) {
		if (nlines >= maxlines || ( p+len > linestop ))
			return -1;
		else {
			line[len-1] = '\0';
			strcpy(p, line);
			lineptr[nlines++] = p;
			p += len;
		}
	}
	return nlines;
}

void
writelines (char *lineptr[], int nlines) {
	
	while ( nlines-- > 0 )
		printf("%s\n", *lineptr++ );

}

int 
mgetline (char *s, int n) {

	char c;
	char *p;

	p = s;

	while ( n-- > 0 && ( c = getchar() ) != EOF && c != '\n' ) *s++ = c;

	if ( c == '\n' ) *s++ = c;

	*s = '\0';

	return s - p;

}

void
qsort (char *v[], int l, int r) {

	int i, last;
	
	if ( l >= r ) return;

	swap( v, l, (l+r)/2);

	last = l;

	for ( i = l+1; i <= r; i++ ) {
		if (strcmp(v[i], v[l]) < 0 )
			swap(v, ++last, i);
	}

	swap(v, l, last);
	qsort(v, l, last-1);
	qsort(v, last+1, r);

}

void
swap (char *v[], int i, int j) {

	char *tmp;

	tmp = v[i];
	v[i] = v[j];
	v[j] = tmp;
	
}

void
main (int argc, char *argv[]) {

	int nlines;

	if (( nlines = readlines(lineptr, linestore, MAXLINES)) >= 0 ) {
		qsort(lineptr,0, nlines-1);
		writelines(lineptr, nlines);
	} else {
		printf("error: input too big to sort\n");
	}

}

