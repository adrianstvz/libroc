#include <stdio.h>
#include <stdlib.h>

void
main (int argc, char *argv[]){

	double nl = 0;
	int c;

	while ( (c=getchar()) != EOF ){
		if (c=='\n') nl++;
	}
	printf("%.0f Lines\n", nl);

}
