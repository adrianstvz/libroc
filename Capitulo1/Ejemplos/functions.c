#include <stdio.h>
#include <stdlib.h>

int power (int, int);

/* power: raise base to the n-th power */
int
power (int base, int n) {

	int p;

	for (p=1; n>0; n--) {
		p = p*base;
	}

	return p;
}

void
main (int argc, char *argv[]){

	int i;

	for (i=0; i<10; i++) {
		printf("%d %d %d\n", i, power(2,i), power(-3,i));
	}

}

