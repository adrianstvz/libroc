#include <stdio.h>
#include <stdlib.h>

#define IN 1
#define OUT 0

void
main (int argc, char *argv[]){

	int c, state;
	long nc, nw, nl;

	nc = nw = nl = 0;
	state = OUT;

	for(c = 0; c != EOF; c=getchar()){
		nc++;

		if(c == '\n') nl++;

		if( c == ' ' || c == '\t' || c == '\n' ) state = OUT;	
		else if ( state == OUT ) {
			state = IN;
			nw++;
		}
	}

	printf("characters: %ld, words: %ld, lines: %ld\n", nc, nw, nl);

}

