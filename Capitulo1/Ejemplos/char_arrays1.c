#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000

int max;
char line[MAXLINE];
char longest[MAXLINE];

int mgetline ();
void copy ();

int
mgetline () {
/* This function reads a line from input into s */

	int c, i;
	extern char line[];

	for ( i=0; i < MAXLINE-1 && (c=getchar()) != EOF && c != '\n'; i++) {
		line[i]=c;
	}
	if (c=='\n') {
		line[i++] = c;
	}

	line[i] = '\0';

	return i;

}

void
copy () {
/* This function copies a string 'from' 'to' */

	int i = 0;
	extern char line[], longest[];

	while (	(longest[i] = line[i]) != '\0') i++;

}

void
main (int argc, char *argv[]) {

	int len;
	extern int max;
	extern char longest[];

	max = 0;

	while ( (len = mgetline()) > 0 ) {
		if (len > max) {
			max = len;
			copy();
		}
	}

	if (max > 0) {
		 printf("%s", longest);
	}

}

