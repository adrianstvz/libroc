#include <stdio.h>
#include <stdlib.h>

/*
 * Correct version of the Fahrenheit-Celsius table
 */
void
main (int argc, char *argv[]){

	for (int i=0; i<300; i+=20){
		printf("%3d F %6.1f C\n", i, (double) 5/9 * (i-32) );
	}

}

