#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000

int mgetline (char *, int);
void copy (char *, char *);

int
mgetline (char *s, int max_length) {
/* This function reads a line from input into s */

	int c, i;

	for ( i=0; i < max_length-1 && (c=getchar()) != EOF && c != '\n'; i++) {
		s[i]=c;
	}
	if (c=='\n') {
		s[i++] = c;
	}

	s[i] = '\0';

	return i;

}

void
copy (char *to, char *from) {
/* This function copies a string 'from' 'to' */

	int i = 0;

	while (	(to[i] = from[i]) != '\0') i++;

}

void
main (int argc, char *argv[]) {

	int len, max;
	char line[MAXLINE];
	char longest[MAXLINE];

	max = 0;

	while ( (len = mgetline(line, MAXLINE)) > 0 ) {
		if (len > max) {
			max = len;
			copy(longest, line);
		}
	}

	if (max > 0) {
		 printf("%s", longest);
	}

}

