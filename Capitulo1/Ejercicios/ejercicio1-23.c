#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000
#define LIMIT 30
#define TS 4

#define OUT 0
#define IN 1
#define START 2
#define END 3

int comment_state = OUT;

int mgetline (char *);
void detab (char *);
void entab (char *);
void folder (char *);
void decomment (char *);

int 
mgetline (char *s) {
/* This function copies a line from input into an array */

	int i;
	char c;

	for (i=0; (i < MAXLINE - 1) && ( ( c = getchar() ) != EOF ) && ( c != '\n' ) ; i++) {
		s[i] = c;
	}

	if ( c == '\n' ) {
		s[i++] = c;
	}

	s[i] = '\0';

	return i;
}

void
detab (char *s) {
/* This function replaces tabs with N blanks */
	
	int i, j, len, jump = TS-1 ;

	// Get lenght of array
	for ( len = 0; s[len] != '\0'; len++)

	for ( i=0; s[i] != '\0'; i++ ) {
		if ( s[i] == '\t' ) {
			len += jump;

			// Move the string forward from current position
			for ( j = len; j > i+jump ; j--) s[j] = s[j-jump];
			
			// Fill the spaces with blanks
			for ( j=0; j <= jump; j++ ) s[i+j] = ' ';
		}
	}
}

void
entab (char *s) {
/* This function replaces N spaces with a tab */
	
	int i, j, count, len, pos, jump = TS-1 ;
	int state = OUT;

	count = 0;

	// Get length of array
	for ( len = 0; s[len] != '\0'; len++);

	for ( i=0; s[i] != '\0'; i++ ) {
		if ( s[i] == ' ' ) {
			state = IN;
			count++;
		} else if ( state == IN ) {
			// Set the starting position
			pos = i - count;
			while (count >= TS ) {
				jump = TS - 1;

				s[pos] = '\t';

				// Move the string backward from current position
				for ( j = ++pos; j <= (len - jump); j++){
					s[j] = s[j+jump];
				}

				count -= TS;
			}
			state = OUT;
			count = 0;
		}
	}
}

void
folder (char *s) {
/* This function folds long lines into shorter ones */
	
	int i, j, len, spaces, state;

	int last_space, line_size;

	last_space = line_size = 0;
	state = OUT;
	spaces = 0;

	for ( i=0; s[i] != '\0'; i++ ) {

		line_size++;

		// Save position of begining of last blank string
		if ( s[i] == ' ' || s[i] == '\t' ) {
			if ( state == OUT ) {
				state = IN;
				spaces = 0;
				last_space = i;
			}
			spaces++;
		} else {
			state = OUT;

			if ( line_size > LIMIT && spaces > 0) {
				// Replace last space with newline
				s[last_space++] = '\n';
		
				// Remove following spaces
				if (spaces-- > 1) {
					for (j=last_space; s[j+spaces] != '\0'; j++ ) s[j] = s[j+spaces];
					s[j] = '\0';
				}

				line_size = 0;
				i = last_space;

				printf("%s", s);
			}
		}

		//printf("%d %d %d\n", i, line_size, last_space);

	}
}
void
decomment (char *s) {
/* // This function removes comments from a line //  
 * // asda sd/// da
 *
 * sdasdasda */ //sdasdas/ asdasdas 
	
	int i, j, jump, pos = 0, end = 0;

	extern int comment_state;

	for ( i=0; s[i] != '\0'; i++ ) {
		if ( s[i] == '/' ) {
			if ( comment_state == OUT ) {
				comment_state = START;
				pos = i;
			}
			else if ( comment_state == START ) {
				comment_state = OUT;
				s[pos+1] = '\0';
				if ( pos > 0 ) s[pos] = '\n';
				break; 
			}
			else if ( comment_state == END ) {
				comment_state = OUT;

				// Remove everything from pos to current position
				jump = (i - pos) + 1;
				for ( j=pos; j<=i; j++) s[j] = s[j+jump];
				
				// Reset index
				i = pos;
			}
		}

		else if ( s[i] == '*' ) {
			if ( comment_state == START )
				comment_state = IN;

			else if ( comment_state == IN )
				comment_state = END; // TEST COMMMENT 
		}

		else if ( comment_state == START ) 
			comment_state = OUT;

		else if ( comment_state == END ) 
			comment_state = IN;
	}
	/* THIS GETS DELETED 
	 * */ 

	if ( comment_state == IN ) {
		if ( pos > 0 ) s[pos++] = '\n';
		s[pos] = '\0';
	}
}

void
main (int argc, char *argv[]){

	int len;

	char line[MAXLINE];

	while ( len = mgetline(line) ) {
		decomment(line);
		printf("%s",line);
	}
}
