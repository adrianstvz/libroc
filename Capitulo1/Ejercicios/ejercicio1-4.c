#include <stdio.h>
#include <stdlib.h>

/*
 * Correct version of the Celsius-Fahrenheit table
 */
void
main (int argc, char *argv[]){

	printf("%10s %10s\n", "CELSIUS", "FAHRENHEIT");
	for (int i=-20; i<100; i+=10){
		printf("%10d %10.1f\n", i, (double) 9/5 * i + 32);
	}

}

