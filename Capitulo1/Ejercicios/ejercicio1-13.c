#include <stdio.h>
#include <stdlib.h>

#define IN 1
#define OUT 0
#define SIZE 20

void
main (int argc, char *argv[]){

	int c, i, j, max, len, word;
	int counter[SIZE];

	max = len = 0;
	word = OUT;

	for (i=0; i<SIZE; i++)
		counter[i] = 0;

	while ( (c=getchar()) != EOF ) {
		if ( c == ' ' || c == '\n' || c == '\t' ) {
			if ( word == IN ) {
				if (max < len) max = len;
				counter[len]++;
				len = 0;
				word = OUT;
			}
		} else {
			word = IN;
			len++;
		}
	}

	for (i=0; i<max; i++) {
		printf("%3d: ",i);
		for (j=0; j<counter[i]; j++) {
			putchar('#');
		}
		putchar('\n');
	}
}

