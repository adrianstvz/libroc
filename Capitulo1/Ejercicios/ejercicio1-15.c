#include <stdio.h>
#include <stdlib.h>

double fahrenheit_to_celsius (int);
double celsius_to_fahrenheit (int);


double
fahrenheit_to_celsius (int f) {
/* This function converts Fahrenheit to Celsius */
	return (double) 5/9 * (f-32);
}

double
celsius_to_fahrenheit (int c) {
/* This function converts Celsius to Fahrenheit */
	return (double) 9/5 * c + 32;
}

void
main (int argc, char *argv[]){

	printf("%10s %10s\n", "FAHRENHEIT", "CELSIUS");
	for (int i=0; i<300; i+=20){
		printf("%10d %10.1f\n", i, fahrenheit_to_celsius(i) );
	}

	putchar('\n');
	printf("%10s %10s\n", "CELSIUS", "FAHRENHEIT");
	for (int i=-20; i<100; i+=10){
		printf("%10d %10.1f\n", i, celsius_to_fahrenheit(i) );
	}

}

