#include <stdio.h>
#include <stdlib.h>

void
main (int argc, char *argv[]){

	int c;
	long nl, nb, nt;
	nl = 0;
	nb = 0;
	nt = 0;

	while( (c = getchar()) != EOF ){
		if(c=='\n') nl++;
		if(c=='\t') nt++;
		if(c==' ') nb++;
	}

	printf("blanks: %ld, tabs: %ld, lines: %ld\n", nb, nt, nl);

}

