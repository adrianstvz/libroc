#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000

int mgetline (char *, int);
void reverse (char *);

int
mgetline (char *s, int max_length) {
/* This function reads a line from input into s */

	int c, i;

	for ( i = 0; ( i < max_length-1 ) &&  ( c = getchar() ) != EOF && c != '\n'; i++) {
		s[i] = c;
	}

	if ( c == '\n' ) {
		s[i++] = c;
	} 

	s[i] = '\0';

	return i;

}

void
reverse (char *s) {
/* This function reverses a character string */

	int i, len = 0;
	int tmp;

	while (	s[len] != '\0') len++;
	
	if ( s[--len] == '\n' ) len--;

	for ( i=0; i < (len + 1) / 2 ; i++) {
		tmp = s[i];
		s[i] = s[len - i];
		s[len - i] = tmp;
	}

}

void
main (int argc, char *argv[]) {

	int len, max;
	char line[MAXLINE];
	char longest[MAXLINE];

	max = 0;

	while ( (len = mgetline(line, MAXLINE)) > 0 ) {
		reverse(line);
		printf("%s", line);
	}

}

