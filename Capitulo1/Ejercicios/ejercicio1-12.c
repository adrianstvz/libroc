#include <stdio.h>
#include <stdlib.h>

#define IN 1
#define OUT 0

void
main (int argc, char *argv[]){

	int c, state = 0;

	for(c = 0; c != EOF; c=getchar()){

		if( c == ' ' || c == '\t' || c == '\n'){
			if ( state == IN ) {
				putchar('\n');	
				state = OUT;
			}
		} else {
			putchar(c);
			state = IN;
		}
	}

}

