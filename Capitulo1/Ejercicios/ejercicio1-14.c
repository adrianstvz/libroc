#include <stdio.h>
#include <stdlib.h>

#define SIZE 128

void
main (int argc, char *argv[]){

	int c, i, j, max_char;

	int count[SIZE];

	for (i=0; i<SIZE; i++) {
		count[i]=0;
	}

	while ( (c=getchar()) != EOF ) {
		count[c]++;
	}

	for (i=0; i<SIZE; i++) {

		if ( count[i] > 0 ) {
			if ( i == '\t' ) printf("\\t");
			else if (i == ' ') printf("\\b");
			else if (i == '\n') printf("\\n");
			else putchar(i);
			printf(": ");
			for ( j=0; j<count[i]; j++) printf("#");
			putchar('\n');
		}
	}

}

