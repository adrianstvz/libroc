#include <stdio.h>
#include <stdlib.h>

/*
 * Correct version of the Fahrenheit-Celsius table
 */
void
main (int argc, char *argv[]){

	printf("%10s %10s\n", "FAHRENHEIT", "CELSIUS");
	for (int i=0; i<300; i+=20){
		printf("%10d %10.1f\n", i, (double) 5/9 * (i-32) );
	}

}

