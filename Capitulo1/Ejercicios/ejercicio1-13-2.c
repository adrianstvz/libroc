#include <stdio.h>
#include <stdlib.h>

#define IN 1
#define OUT 0
#define SIZE 20

void
main (int argc, char *argv[]){

	int c, i, j, max_char, max_length, len, word;
	int counter[SIZE];

	max_length = max_char = len = 0;
	word = OUT;

	for (i=0; i<SIZE; i++)
		counter[i] = 0;

	while ( (c=getchar()) != EOF ) {
		if ( c == ' ' || c == '\n' || c == '\t' ) {
			if ( word == IN ) {
				if (max_char < len) max_char = len;
				counter[len]++;
				len = 0;
				word = OUT;
			}
		} else {
			word = IN;
			len++;
		}
	}

	for (i=0; i<max_char; i++) {
		if (counter[i]>max_length) max_length = counter[i];
	}

	for (i=max_length; i>0; i--){
		for (j=0; j<max_char; j++) {
			if ( counter[j] < i) printf("%3s", " ");
			else printf("%3s", "#");
		}
		putchar('\n');
	}

	for (i=0; i<max_char; i++) {
		printf("%3d", i);
	}

	putchar('\n');

}

