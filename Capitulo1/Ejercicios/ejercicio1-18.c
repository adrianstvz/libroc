#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000
#define BEFORE 0
#define AFTER 1

int mgetline (char *, int);

int
mgetline (char *s, int max_length) {
/* This function reads a line from input into s */

	int c, i, len = 0;
	int pos = BEFORE;

	for ( i = 0; ( len < max_length - 1 ) && ( c = getchar() ) != EOF && c != '\n'; i++) {
		if ( pos == BEFORE ) {
			if ( c == '\t' || c == ' ' ) ;
			else {
				pos = AFTER;
				s[len++] = c;
			}
		} else {
			s[len++] = c;
		}

	}

	if ( c == '\n' ) {
		s[len++] = c;
		i++;
	} 
	s[len] = '\0';

	return i;

}

void
main (int argc, char *argv[]) {

	int len, max;
	char line[MAXLINE];

	max = 0;

	while ( (len = mgetline(line, MAXLINE)) > 0 ) {
		if (len > 1) {
		 	printf("%s", line);
		}
	}

}

