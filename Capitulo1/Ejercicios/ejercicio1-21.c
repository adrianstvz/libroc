#include <stdio.h>
#include <stdlib.h>

#define MAXLINE 1000
#define TS 4

#define OUT 0
#define IN 1

int mgetline (char *);
void detab (char *);
void entab (char *);

int 
mgetline (char *s) {
/* This function copies a line from input into an array */

	int i;
	char c;

	for (i=0; (i < MAXLINE - 1) && ( ( c = getchar() ) != EOF ) && ( c != '\n' ) ; i++) {
		s[i] = c;
	}

	if ( c == '\n' ) {
		s[i++] = c;
	}

	s[i] = '\0';

	return i;
}

void
detab (char *s) {
/* This function replaces tabs with N blanks */
	
	int i, j, len, jump = TS-1 ;

	// Get lenght of array
	for ( len = 0; s[len] != '\0'; len++)

	for ( i=0; s[i] != '\0'; i++ ) {
		if ( s[i] == '\t' ) {
			len += jump;

			// Move the string forward from current position
			for ( j = len; j > i+jump ; j--) s[j] = s[j-jump];
			
			// Fill the spaces with blanks
			for ( j=0; j <= jump; j++ ) s[i+j] = ' ';
		}
	}
}

void
entab (char *s) {
/* This function replaces N spaces with a tab */
	
	int i, j, count, len, pos, jump = TS-1 ;
	int state = OUT;

	count = 0;

	// Get length of array
	for ( len = 0; s[len] != '\0'; len++);

	for ( i=0; s[i] != '\0'; i++ ) {
		if ( s[i] == ' ' ) {
			state = IN;
			count++;
		} else if ( state == IN ) {
			// Set the starting position
			pos = i - count;
			while (count >= TS ) {
				jump = TS - 1;

				s[pos] = '\t';

				// Move the string backward from current position
				for ( j = ++pos; j <= (len - jump); j++){
					s[j] = s[j+jump];
				}

				count -= TS;
			}
			state = OUT;
			count = 0;
		}
	}
}

void
main (int argc, char *argv[]){

	int len;

	char line[MAXLINE];

	while ( len = mgetline(line) ) {
		entab(line);
		printf("%s",line);
	}
}

