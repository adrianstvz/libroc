#include  <stdio.h>

#define MAXLINE 1000
#define OUT 0
#define IN 1

int mgetline (char *, int) ;
void expand (char *, char *) ;

int
mgetline (char *s, int lim) {
	
	int i, c;

	for ( i=0; (c = getchar()) != EOF  && i < lim; i++ ) {
		s[i] = c;

		if (c == '\n') {
			i++;
			break;
		}
	}

	s[i] = '\0';

	return i;
}

void
expand (char *s, char *t) {

	int i, j, state = OUT;

	char c, first = 0;

	printf("\n");
	for ( i = j = 0; ( c = s[i] ) != '\0'; i++ ){

		if ( state == IN ) {
		
			while ( first < c ) t[j++] = ++first;

			state = OUT;

		} else {

			if ( c == '-' && first > 0 ) state = IN;
			else {
				first = c;
				t[j++] = first;
				state = OUT;
			}
			
		}

	}

	t[j] = '\0';
}

void
main () {

	char line[MAXLINE], expanded[MAXLINE];

	while ( mgetline(line, MAXLINE)) {
		expand(line, expanded);

		printf("%s", expanded);
	}

}
