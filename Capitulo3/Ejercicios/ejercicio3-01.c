#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#define N_TESTS 1000

int binsearch (int, int *, int) ;
int nbinsearch (int, int *, int) ;

double microseconds() {   
	struct timeval t;
	if (gettimeofday(&t, NULL) < 0 ) return 0.0;
	return (t.tv_usec + t.tv_sec * 1000000.0);
}

int 
binsearch (int x, int *v, int n) {

	int low, high, mid;

	low = 0;
	high = n-1;
	while ( low <= high ) {
		mid = (high + low) / 2;

		if ( x == v[mid] ) return mid;			// MATCH
		else if ( x > v[mid] ) low = mid + 1;	// Item is on higher mid
		else high = mid - 1;					// Item is on lower mid
	}

	return -1;
}

int 
nbinsearch (int x, int *v, int n) {

	int low, high, mid;

	low = 0;
	high = n-1;
	mid = (high + low) / 2;

	while ( (low <= high) && (x != v[mid]) ) {
		//printf("%d, %d - v[%d] = %d\n", low, high, mid, v[mid]);

		if ( x > v[mid] ) low = mid + 1;	// Item is on higher mid
		else high = mid - 1;				// Item is on lower mid

		mid = (high + low) / 2;
	}

	if ( x == v[mid] ) return mid;			// MATCH
	else return -1;
}

void
main (int argc, char *argv[]){

	int i, n, size=15 ;
	int v[15] = {0,4,7,9,11,13,17,18,21,25,27,31,33,35,37} ;

	int seed = time(NULL);
	
	double t1, t2;

	srand(seed);
	t1 = microseconds();
	for (i = 0; i < N_TESTS; i++){
		n = rand() % 50;
		binsearch(n,v,size);
	}
	t1 = microseconds() - t1;

	srand(seed);
	t2 = microseconds();
	for (i = 0; i < N_TESTS; i++){
		n = rand() % 10;
		nbinsearch(n,v,size);
	}
	t2 = microseconds() - t2;

	printf("T1: %.1fμs\nT2: %.1fμs\n", t1, t2);

}

