#include <stdio.h>

#define MAXLINE 1000

void mitoa (int, char *, int) ;
void reverse (char *) ;

void
mitoa (int n, char *s, int minsize) {

	int i, sign;

	sign = ( n<0 ) ? -1 : 1;

	i = 0;

	do {
		// We use actual value and multiply by sign
		s[i++] = sign * (n%10) + '0';
	} while ( (n /= 10) != 0 );

	if ( sign < 0 ) s[i++] = '-' ;

	while ( i < minsize ) s[i++] = ' ';

	s[i] = '\0';
	reverse(s);
}

void 
reverse (char *s) {
	
	int c, i, j;

	for ( j = 0; s[j] != '\0'; j++ );

	for ( i = 0, j-- ; i<j; i++, j-- ) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
		
}

void
main (int argc, char *argv[]) {

	int i;
	char s[MAXLINE];

	i = -12451;
	mitoa(i,s,0);
	printf("%d -> %s\n", i, s);

	mitoa(i,s,4);
	printf("%d -> %s\n", i, s);

	mitoa(i,s,10);
	printf("%d -> %s\n", i, s);

	mitoa(i,s,MAXLINE);
	printf("%d -> %s\n", i, s);

}

