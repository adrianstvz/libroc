#include  <stdio.h>

#define MAXLINE 1000
#define OUT 0
#define IN 1

int mgetline(char *, int) ;
int escape(char *, char *) ;
int unescape(char *, char *) ;

int
mgetline (char *s, int lim) {
	
	int i, c;

	for ( i=0; (c = getchar()) != EOF  && i < lim; i++ ) {
		s[i] = c;

		if (c == '\n') {
			i++;
			break;
		}
	}

	s[i] = '\0';

	return i;
}

int
escape (char *s, char *t) {

	int i, j, c;

	for (i = j = 0; ( c = t[i] ) != '\0'; i++) {
		switch(t[i]) {
			case '\t':			// If tab replace with "\t"
				s[j++] = '\\';
				s[j] = 't';
				break;
			case '\n':			// If newline replace with "\n"
				s[j++] = '\\';
				s[j] = 'n';
				break;
			default:			// Other add the character
				s[j] = c;
				break;
		}
		j++;
	}

	s[j] = '\0';

}

int
unescape (char *s, char *t) {

	int i, j, c;
	int state = OUT;

	for (i = j = 0; ( c = t[i] ) != '\0'; i++) {
		switch(t[i]) {
			case 'n':	
				if ( state == IN ){
					s[--j] = '\n';	// Overwrite backslash
					state = OUT;
				} else {
					s[j] = c;
				}
				break;
			case 't':
				if ( state == IN ){
					s[--j] = '\t';
					state = OUT;
				} else {
					s[j] = c;
				}
				break;
			case '\\':			// If is trailing backslash then IN, else OUT
				state = ( state == OUT );
			default:			// Other add the character
				s[j] = c;
				break;
		}
		j++;
	}

	s[j] = '\0';

}

void
main () {

	char line[MAXLINE], escaped[MAXLINE], unescaped[MAXLINE];

	while ( mgetline(line, MAXLINE)) {
		escape(escaped, line);
		unescape(unescaped, escaped);

		printf("%s", line);
		printf("%s", escaped);
		printf("%s", unescaped);
	}

}
