#include <stdio.h>

#define MAXLINE 1000

void itob (int, char *, int) ;
void mitoa (int, char *) ;
void reverse (char *) ;

void
itob (int n, char *s, int b) {

	int i, sign, value;

	sign = ( n<0 ) ? -1 : 1;

	i = 0;

	do {
		// We use actual value and multiply by sign
		value = sign * ( n%b );
		s[i++] = value + ((value < 10) ? '0' : 'A' - 10) ;
	} while ( (n /= b) != 0 );

	s[i++] = ( sign < 0 ) ? '-' : '\0';
	s[i] = '\0';
	reverse(s);
}

void
mitoa (int n, char *s) {

	int i, sign;

	sign = ( n<0 ) ? -1 : 1;

	i = 0;

	do {
		// We use actual value and multiply by sign
		s[i++] = sign * (n%10) + '0';
	} while ( (n /= 10) != 0 );

	s[i++] = ( sign < 0 ) ? '-' : '\0';
	s[i] = '\0';
	reverse(s);
}

void 
reverse (char *s) {
	
	int c, i, j;

	for ( j = 0; s[j] != '\0'; j++ );

	for ( i = 0, j-- ; i<j; i++, j-- ) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
		
}

void
main (int argc, char *argv[]) {

	int i;
	char s[MAXLINE];

	i = 15;
	itob(i,s,2);
	printf("%d -> %s\n", i, s);

	itob(i,s,8);
	printf("%d -> %s\n", i, s);

	itob(i,s,10);
	printf("%d -> %s\n", i, s);

	itob(i,s,16);
	printf("%d -> %s\n", i, s);

}

