#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define N_TESTS 10
#define MAXLINE 1000

void mitoa (int, char *);
void reverse (char *) ;

void
mitoa (int n, char *s) {

	int i, sign;

	sign = ( n<0 ) ? -1 : 1;

	i = 0;

	do {
		// We use actual value and multiply by sign
		s[i++] = sign * (n%10) + '0';
	} while ( (n /= 10) != 0 );

	s[i++] = ( sign < 0 ) ? '-' : '\0';
	s[i] = '\0';
	reverse(s);
}

void 
reverse (char *s) {
	
	int c, i, j;

	for ( j = 0; s[j] != '\0'; j++ );

	for ( i = 0, j-- ; i<j; i++, j-- ) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
		
}

void
main (int argc, char *argv[]) {

	int i;
	char s[MAXLINE];

	i = 121879794;
	mitoa(i,s);
	printf("%d -> %s\n", i, s);

	i = -1278924;
	mitoa(i,s);
	printf("%d -> %s\n", i, s);

	i = INT_MIN + 1;
	mitoa(i,s);
	printf("itoa fails with INT_MIN because its sign is changed to positive internally, overflowing the int type, since MAX_INT == -(MIN_INT+1)\n");
	printf("To fix this we don't change the sign of n, instead we use the sign value (-1,1) to multiple the module result\n");
	printf("%d -> %s\n", i, s);

}

