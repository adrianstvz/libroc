#include <stdio.h>

void
main (int argc, char *argv[]){

	int c,i, nw, nd[10], no;

	nw = no = 0;

	for ( i=0; i<10; i++) nd[i] = 0;

	while ( (c=getchar()) != EOF ) {
		switch(c){
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				nd[c-'0']++;
				break;
			case ' ': case '\t': case '\n':
				nw++; break;
			default:
				no++;
				break;
		}
	}

	printf("digits =");
	for (i=0; i<10; i++) printf(" %d%c", nd[i], (i==10-1)?'\n':',');

	printf("white spaces = %d\n", nw);
	printf("others = %d\n", no);

}

