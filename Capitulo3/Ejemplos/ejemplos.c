#include <stdio.h>
#include <limits.h>
#include <string.h>

#define MAXLINE 1000

int matoi (char *) ;
int misblank (char) ;
int misdigit (char) ;
void mitoa (int, char *) ;
void reverse (char *) ;
int trim (char *s) ;

int
misblank (char c) {

	return ( c == ' ' || c == '\n' || c == '\t' );

}

int
misdigit (char c) {

	return ( c >= '0' && c <= '9' );

}

int
matoi (char *s) {

	int i, n, sign;

	for ( i=0; misblank(s[i]) ; i++ );

	sign = ( s[i] == '-' ) ? -1 : 1 ;
	if ( s[i] == '+' || s[i] == '-' ) i++;

	for ( n=0; misdigit(s[i]); i++)
		n = 10 * n + (s[i] - '0');

	return n*sign;
}

void
mitoa (int n, char *s) {

	int i, sign;

	sign = ( n<0 ) ? -1 : 1;

	i = 0;
	n *= sign;

	do {
		s[i++] = n%10 + '0';
	} while ( (n /= 10) > 0 );

	s[i++] = ( sign < 0 ) ? '-' : '\0';
	s[i] = '\0';
	reverse(s);
}

void 
reverse (char *s) {
	
	int c, i, j;

	for ( j = 0; s[j] != '\0'; j++ );

	for ( i = 0, j-- ; i<j; i++, j-- ) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
		
}

int
trim (char *s) {

	int n;

	for ( n = strlen(s)-1; n>=0; n-- )
		if ( ! misblank(s[n]) ) break; 
	
	s[n+1] = '\0';
	return n;
}

void
main (int argc, char *argv[]) {

	int i;
	char s[MAXLINE];

	i = 121879794;
	mitoa(i,s);
	printf("%d -> %s\n", i, s);

	i = -1278924;
	mitoa(i,s);
	printf("%d -> %s\n", i, s);

	i = INT_MIN;
	mitoa(i,s);
	printf("%d -> %s\n", i, s);


	char t[MAXLINE] = "HELLO,    	WORLD!				\n    		 \n  	";
	printf("%sBefore!\n", t);
	trim(t);
	printf("%sAfter!", t);

}

