#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N_TESTS 10

int binsearch (int, int *, int) ;

int 
binsearch (int x, int *v, int n) {

	int low, high, mid;

	low = 0;
	high = n-1;
	while ( low <= high ) {
		mid = (high + low) / 2;

		if ( x == v[mid] ) return mid;			// MATCH
		else if ( x > v[mid] ) low = mid + 1;	// Item is on higher mid
		else high = mid - 1;					// Item is on lower mid
	}

	return -1;
}

void
main (int argc, char *argv[]){

	int v[4] = {0,4,7,9} ;
	int i, n;

	int seed = time(NULL);


	for ( i=0; i<4; i++ ) printf("%d%c", v[i], (i == 4-1) ? '\n' : ',');

	srand(seed);
	for (i = 0; i < N_TESTS; i++){
		n = rand() % 10;
		printf("Number %d is on pos %d\n", n, binsearch(n,v,4));
	}

	printf("End of test 1\n\n");

	srand(seed);
	for (i = 0; i < N_TESTS; i++){
		n = rand() % 10;
		printf("Number %d is on pos %d\n", n, binsearch(n,v,4));
	}
}

