#include <stdio.h>

#define MAXLINE 1000

void reverse (char *) ;

void 
reverse (char *s) {
	
	int c, i, j;

	for ( j = 0; s[j] != '\0'; j++ );

	for ( i = 0, j-- ; i<j; i++, j-- ) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
		
}

void
main (int argc, char *argv[]) {

	char s[MAXLINE] = "Hello, World!";

	printf("%s", s);
	reverse(s);
	printf(" -> %s\n", s);

}

