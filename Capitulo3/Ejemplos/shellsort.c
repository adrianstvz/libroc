#include <stdio.h>
#include <stdlib.h>

#define SIZE 20

void shellsort (int *, int) ;

void
shellsort (int *v, int n) {

	int i, j, gap, temp;

	for ( gap = n/2; gap > 0; gap /= 2 ) {
		for ( i=gap; i<n; i++) {
			for ( j = i-gap; j>=0 && v[j] > v[j+gap]; j -= gap) {
				temp = v[j];
				v[j] = v[j+gap];
				v[j+gap] = temp;
			}
		}
	}
}

void
main (int argc, char *argv[]) {

	int i;
	int vector[SIZE];

	for ( i=0; i<SIZE; i++ ) vector[i] = rand() % SIZE;

	for ( i=0; i<SIZE; i++ ) 
		printf("%d%c", vector[i], (i == SIZE -1)?'\n':',') ;

	shellsort(vector, SIZE);
		
	for ( i=0; i<SIZE; i++ ) 
		printf("%d%c", vector[i], (i == SIZE -1)?'\n':',') ;


}

