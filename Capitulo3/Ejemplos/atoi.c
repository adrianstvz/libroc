#include <stdio.h>

int matoi (char *);
int misblank (char);
int misdigit (char);

int
misblank (char c) {

	return ( c == ' ' || c == '\n' || c == '\t' );

}

int
misdigit (char c) {

	return ( c >= '0' && c <= '9' );

}

int
matoi (char *s) {

	int i, n, sign;

	for ( i=0; misblank(s[i]) ; i++ );

	sign = ( s[i] == '-' ) ? -1 : 1 ;
	if ( s[i] == '+' || s[i] == '-' ) i++;

	for ( n=0; misdigit(s[i]); i++)
		n = 10 * n + (s[i] - '0');

	return n*sign;
}

void
main (int argc, char *argv[]){

	printf("%d\n", matoi("      	-1213123 98123") );

	printf("%d\n", matoi("      	-12131 2398123") );

	printf("%d\n", matoi("      	12131 2398123") );

}

