# THE C PROGRAMMING LANGUAGE
## Ejemplos y Ejercicios

Aunque llevo tiempo programando en C y consultando este libro temporalmente, nunca me había parado a leerlo detenida e íntegramente.

Aquí incluyo tanto los ejemplos aportados en el libro como la solución a todos sus ejercicios.

- **[Capítulo 1:](Capitulo1)** Introducción
- **[Capítulo 2:](Capitulo2)** Tipos, Operadores y Expresiones
- **[Capítulo 3:](Capitulo3)** Control de Flujo
- **[Capítulo 4:](Capitulo4)** Estructura de Funciones y Programas
- **[Capítulo 5:](Capitulo5)** Punteros y Matrices
- **[Capítulo 6:](Capitulo6)** Estructuras
- **[Capítulo 7:](Capitulo7)** Entrada y Salida
- **[Capítulo 8:](Capitulo8)** Interfaz del Sistema UNIX
